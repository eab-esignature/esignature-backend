package com.eab.common;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

//import org.apache.log4j.Logger;
public class eSignSessionListener implements HttpSessionListener {
//	private Logger logger = Logger.getLogger(eSignSessionListener.class);
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		// TODO Auto-generated method stub
		eSignSessionContext.AddSession(se.getSession());

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		// TODO Auto-generated method stub
		eSignSessionContext.DelSession(se.getSession());
	}

}
