package com.eab.pdf;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.pobjects.graphics.text.LineText;
import org.icepdf.core.pobjects.graphics.text.PageText;
import org.icepdf.core.pobjects.graphics.text.WordText;
import org.icepdf.core.util.GraphicsRenderingHints;
import org.json.JSONObject;

public class IcepdfConvertor extends PDFConvertor {
	private Logger logger = Logger.getLogger(IcepdfConvertor.class);

	public IcepdfConvertor() throws IOException {
		super();
	}

//	public boolean convertPDFtoIMG(String sessionID)
//			throws PDFException, PDFSecurityException, IOException, InterruptedException {
//		logger.debug("convertion start");
//		String filePath = this.fileAnalysisLocation + "\\" + sessionID+ "\\"+this.fileAnalysisSrcLocatoin+"\\eSignatureSrc.pdf";
//		Document document = new Document();
//		document.setFile(filePath);
//		float scale = Float.valueOf(this.imageScaling); // 缩放比例
//		float rotation = Float.valueOf(this.imageRotation); // 旋转角度
//		String workingFolder = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisWorkingLocatoin;
//		for (int i = 0; i < document.getNumberOfPages();i++) {
//			BufferedImage image = (BufferedImage) document.getPageImage(i, GraphicsRenderingHints.SCREEN,
//					Page.BOUNDARY_CROPBOX, rotation, scale);
//			RenderedImage rendImage = image;
//			File file = new File(workingFolder + "\\eSignature_" + (i+1) + ".jpg");
//			ImageIO.write(rendImage, "png", file);
//			image.flush();
//		}
//		this.setTotPageCnt(document.getNumberOfPages());
//		document.dispose();
//		logger.debug("convertion end");
//	}

	@Override
	public boolean pdf2img(String srcFileName, String targetFilePath)
			throws PDFException, PDFSecurityException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		boolean success = false;
		try {
		Document document = new Document();
		document.setFile(srcFileName);
		float scale = Float.valueOf(this.imageScaling); // 缩放比例
		float rotation = Float.valueOf(this.imageRotation); // 旋转角度
		double pageHeight = 0.0f;
		double pageWidth = 0.0f;
		for (int i = 0; i < document.getNumberOfPages();i++) {
			BufferedImage image = (BufferedImage) document.getPageImage(i, GraphicsRenderingHints.SCREEN,
					Page.BOUNDARY_CROPBOX, rotation, scale);
			Page page = document.getPageTree().getPage(i);
			pageHeight = page.getSize(0).getHeight();
			pageWidth = page.getSize(0).getWidth();
			RenderedImage rendImage = image;
			File file = new File(targetFilePath + "\\eSignature_" + (i+1) + ".jpg");
			ImageIO.write(rendImage, "png", file);
			image.flush();
		}
		this.getConvertResult().setTotPageCnt(document.getNumberOfPages());
		this.getConvertResult().setPageHeight(Float.valueOf(String.valueOf(pageHeight)));
		this.getConvertResult().setPageWidth(Float.valueOf(String.valueOf(pageWidth)));
		document.dispose();
		success = true;
		}catch(Exception ex) {
			logger.error(ex.toString());
			success = false;
			throw ex;
		}
		return success;
	}

//	public boolean saveImagesToHD(List<BufferedImage> images,String sessionID) throws IOException {
//		if(images!=null && images.size()>0) {
//			String workingFolder = this.fileAnalysisLocation+"\\"+sessionID+"\\"+this.fileAnalysisWorkingLocatoin;
//			for(int i=0;i<images.size();i++) {
//				RenderedImage rendImage=images.get(i);
//				File file=new File(workingFolder+"\\eSignature_"+(++i)+".jpg");// png：格式是jpg但有png清晰度 				
//				ImageIO.write(rendImage,"png",file);
//			}
//		}
//		return true;
//	}
	public void textLocalization(String sessionID)
			throws PDFException, PDFSecurityException, IOException, InterruptedException {
//		File filePath=new File("f:/pdfconvertion/demo_chi.pdf");	
		Document document = new Document();
		document.setFile("f:/pdfconvertion/demo_chi.pdf");

//		document.setFile(filePath.getCanonicalPath());

		PageText pageText;
		StringBuilder pageBuffer = null;
		for (int pageIdx = 0; pageIdx < document.getNumberOfPages(); pageIdx++) {
			pageBuffer = new StringBuilder();
			pageText = document.getPageText(pageIdx);
			float pageHeight = (float) document.getPageDimension(pageIdx, 0f).getHeight();
			float pageWidth = (float) document.getPageDimension(pageIdx, 0f).getWidth();
			pageBuffer.append("pageWidth=" + pageWidth + ";");
			pageBuffer.append("pageHeight=" + pageHeight + ";");
			System.out.println("page:" + pageIdx + " pageWidth:" + pageWidth + " pageHeight:" + pageHeight);
			int lineIdx = 0;
			for (Object olineText : pageText.getPageLines()) {

				LineText lineText = (LineText) olineText;

				// System.out.println("line:"+lineIdx+" x:
				// "+returnWithPrecision(lineText.getBounds().getX())+" maxx:
				// "+returnWithPrecision(lineText.getBounds().getMaxX())+" y:
				// "+returnWithPrecision(lineText.getBounds().getY())+" width:
				// "+returnWithPrecision(lineText.getBounds().getWidth())+" height:
				// "+returnWithPrecision(lineText.getBounds().getHeight()));

				for (WordText owordText : lineText.getWords()) {
					if (lineIdx > 0 && lineIdx < 5)
						System.out.println(owordText.getText());
//			System.out.println("wordX:"+((WordText)owordText).getBounds().getX()+" wordY:"+((WordText)owordText).getBounds().getY());
				}
				lineIdx++;
			}
		}
	}
	@Override
	public void analyzePDF(String sessionID) throws Exception {
		PDFBoxConvertor pDFBoxConvertor = new PDFBoxConvertor();
		pDFBoxConvertor.seteSignatureRule(eSignatureRule);
		pDFBoxConvertor.analyzePDF(sessionID);
		int totPageCnt = this.getConvertResult().getTotPageCnt();
		float pageHeight = this.getConvertResult().getPageHeight();
		float pageWidth = this.getConvertResult().getPageWidth();
		this.setConvertResult(pDFBoxConvertor.getConvertResult());
		this.getConvertResult().setPageWidth(pageWidth);
		this.getConvertResult().setPageHeight(pageHeight);
		this.getConvertResult().setTotPageCnt(totPageCnt);
		
	}

	@Override
	public boolean mergeSignToPDF(String sessionID, JSONObject jsonObject) throws Exception {
		PDFBoxConvertor pDFBoxConvertor = new PDFBoxConvertor();
		pDFBoxConvertor.seteSignatureRule(eSignatureRule);
		return pDFBoxConvertor.mergeSignToPDF(sessionID, jsonObject);
	}

	@Override
	public List<float[]> findKeyWordPosition(String lookupKey, String filePath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean signPDF(String sessionID, JSONObject jsonObject) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pdf2imgAsync(String srcFileName, String targetFilePath) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void analyzePDFAsync(String sessionID) throws Exception {
		// TODO Auto-generated method stub
		
	}


}
