// Decompiled by DJ v2.9.9.60 Copyright 2000 Atanas Neshkov  Date: 12/5/2019 3:14:12 PM
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   CreateVisibleSignature.java

package com.eab.pdf.digitalsignature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Calendar;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.examples.signature.CreateSignatureBase;
import org.apache.pdfbox.examples.signature.SigUtils;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.ExternalSigningSupport;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDVisibleSigProperties;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDVisibleSignDesigner;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.apache.pdfbox.util.Hex;

import com.eab.common.EnvVariable;

// Referenced classes of package org.apache.pdfbox.examples.signature:
//            CreateSignatureBase, SigUtils

public class CreateVisibleSignature extends CreateSignatureBase
{

    public boolean isLateExternalSigning()
    {
        return lateExternalSigning;
    }

    public void setLateExternalSigning(boolean lateExternalSigning)
    {
        this.lateExternalSigning = lateExternalSigning;
    }

    public void setVisibleSignDesigner(String filename, int x, int y, int zoomPercent, InputStream imageStream, int page)
        throws IOException
    {
        visibleSignDesigner = new PDVisibleSignDesigner(filename, imageStream, page);
        visibleSignDesigner.xAxis(x).yAxis(y).zoom(zoomPercent).adjustForRotation();
    }

    public void setVisibleSignDesigner(int zoomPercent, InputStream imageStream)
        throws IOException
    {
        visibleSignDesigner = new PDVisibleSignDesigner(imageStream);
        visibleSignDesigner.zoom(zoomPercent);
    }

    public void setVisibleSignatureProperties(String name, String location, String reason, int preferredSize, int page, boolean visualSignEnabled)
    {
        visibleSignatureProperties.signerName(name).signerLocation(location).signatureReason(reason).preferredSize(preferredSize).page(page).visualSignEnabled(visualSignEnabled).setPdVisibleSignature(visibleSignDesigner);
    }

    public void setVisibleSignatureProperties(String name, String location, String reason, boolean visualSignEnabled)
    {
        visibleSignatureProperties.signerName(name).signerLocation(location).signatureReason(reason).visualSignEnabled(visualSignEnabled).setPdVisibleSignature(visibleSignDesigner);
    }

    public CreateVisibleSignature(KeyStore keystore, char pin[])
        throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, IOException, CertificateException
    {
        super(keystore, pin);
        lateExternalSigning = false;
    }

    public void signPDF(File inputFile, File signedFile, String tsaUrl)
        throws IOException
    {
        signPDF(inputFile, signedFile, tsaUrl, null);
    }

    public void signPDF(File inputFile, File signedFile, String tsaUrl, String signatureFieldName)
        throws IOException
    {
        if(inputFile == null || !inputFile.exists())
            throw new IOException("Document for signing does not exist");
        setTsaUrl(tsaUrl);
        FileOutputStream fos = new FileOutputStream(signedFile);
        PDDocument doc = PDDocument.load(inputFile);
        int accessPermissions = SigUtils.getMDPPermission(doc);
        if(accessPermissions == 1)
            throw new IllegalStateException("No changes to the document are permitted due to DocMDP transform parameters dictionary");
        PDSignature signature = findExistingSignature(doc, signatureFieldName);
        if(signature == null)
            signature = new PDSignature();
        if(doc.getVersion() >= 1.5F && accessPermissions == 0)
            SigUtils.setMDPPermission(doc, signature, 2);
        PDAcroForm acroForm = doc.getDocumentCatalog().getAcroForm();
        if(acroForm != null && acroForm.getNeedAppearances())
            if(acroForm.getFields().isEmpty())
                acroForm.getCOSObject().removeItem(COSName.NEED_APPEARANCES);
            else
                System.out.println("/NeedAppearances is set, signature may be ignored by Adobe Reader");
        signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
        signature.setSubFilter(PDSignature.SUBFILTER_ADBE_PKCS7_DETACHED);
        if(visibleSignatureProperties != null)
        {
            visibleSignatureProperties.buildSignature();
            signature.setName(visibleSignatureProperties.getSignerName());
            signature.setLocation(visibleSignatureProperties.getSignerLocation());
            signature.setReason(visibleSignatureProperties.getSignatureReason());
        }
        signature.setSignDate(Calendar.getInstance());
        SignatureInterface signatureInterface = isExternalSigning() ? null : ((SignatureInterface) (this));
        if(visibleSignatureProperties != null && visibleSignatureProperties.isVisualSignEnabled())
        {
            signatureOptions = new SignatureOptions();
            signatureOptions.setVisualSignature(visibleSignatureProperties.getVisibleSignature());
            signatureOptions.setPage(visibleSignatureProperties.getPage() - 1);
            doc.addSignature(signature, signatureInterface, signatureOptions);
        } else
        {
            doc.addSignature(signature, signatureInterface);
        }
        if(isExternalSigning())
        {
            ExternalSigningSupport externalSigning = doc.saveIncrementalForExternalSigning(fos);
            byte cmsSignature[] = sign(externalSigning.getContent());
            if(isLateExternalSigning())
            {
                externalSigning.setSignature(new byte[0]);
                int offset = signature.getByteRange()[1] + 1;
                RandomAccessFile raf = new RandomAccessFile(signedFile, "rw");
                raf.seek(offset);
                raf.write(Hex.getBytes(cmsSignature));
                raf.close();
            } else
            {
                externalSigning.setSignature(cmsSignature);
            }
        } else
        {
            doc.saveIncremental(fos);
        }
        doc.close();
        IOUtils.closeQuietly(signatureOptions);
    }

    private PDSignature findExistingSignature(PDDocument doc, String sigFieldName)
    {
        PDSignature signature = null;
        PDAcroForm acroForm = doc.getDocumentCatalog().getAcroForm();
        if(acroForm != null)
        {
            PDSignatureField signatureField = (PDSignatureField)acroForm.getField(sigFieldName);
            if(signatureField != null)
            {
                signature = signatureField.getSignature();
                if(signature == null)
                {
                    signature = new PDSignature();
                    signatureField.getCOSObject().setItem(COSName.V, signature);
                } else
                {
                    throw new IllegalStateException((new StringBuilder()).append("The signature field ").append(sigFieldName).append(" is already signed.").toString());
                }
            }
        }
        return signature;
    }

    public static void main(String args[])
        throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException, UnrecoverableKeyException
    {
 //myTest s
//        if(args.length < 4)
//        {
//            usage();
//            System.exit(1);
//        }

//        String tsaUrl = null;
        String tsaUrl = "F:\\pdfconvertion\\convert\\demo_eng.pdf";  //myTest
//        boolean externalSig = false;
//        for(int i = 0; i < args.length; i++)
//        {
//            if(args[i].equals("-tsa"))
//            {
//                if(++i >= args.length)
//                {
//                    usage();
//                    System.exit(1);
//                }
//                tsaUrl = args[i];
//            }
//            if(args[i].equals("-e"))
//                externalSig = true;
//        }
      //myTest e
        String ksFileName="F:\\pdfconvertion\\convert\\keystore.jks";
        String passWord = "ABcd1234";
        String srcPDFFile = "F:\\pdfconvertion\\convert\\demo_eng.pdf";
        String imageFile = "F:\\pdfconvertion\\convert\\timg2.png";
        
//        File ksFile = new File(args[0]);
        File ksFile = new File(ksFileName); //myTest
        KeyStore keystore = KeyStore.getInstance("PKCS12");
//        char pin[] = args[1].toCharArray();
        char pin[] = passWord.toCharArray(); //myTest
        keystore.load(new FileInputStream(ksFile), pin);
//        File documentFile = new File(args[2]);
        File documentFile = new File(srcPDFFile); //myTest
        CreateVisibleSignature signing = new CreateVisibleSignature(keystore, (char[])pin.clone());
//        InputStream imageStream = new FileInputStream(args[3]);
        InputStream imageStream = new FileInputStream(imageFile); //myTest
        String name = documentFile.getName();
        String substring = name.substring(0, name.lastIndexOf('.'));
        File signedDocumentFile = new File(documentFile.getParent(), (new StringBuilder()).append(substring).append("_signed.pdf").toString());
        int page = 1;
//        signing.setVisibleSignDesigner(args[2], 0, 0, -50, imageStream, page); //myTest
        signing.setVisibleSignDesigner(srcPDFFile, 100, 100, -50, imageStream, page);
        imageStream.close();
        signing.setVisibleSignatureProperties("name", "location", "Security", 0, page, true);
//        signing.setExternalSigning(externalSig);
        signing.setExternalSigning(false); //myTest
        signing.signPDF(documentFile, signedDocumentFile, null);
    }


//    private static void usage()
//    {
//        System.err.println((new StringBuilder()).append("Usage: java ").append(com.eab.pdf.CreateVisibleSignature.getName()).append(" <pkcs12-keystore-file> <pin> <input-pdf> <sign-image>\noptions:\n  -tsa <url>    sign timestamp using the given TSA server\n  -e            sign using external signature creation scenario").toString());
//    }

    private SignatureOptions signatureOptions;
    private PDVisibleSignDesigner visibleSignDesigner;
    private final PDVisibleSigProperties visibleSignatureProperties = new PDVisibleSigProperties();
    private boolean lateExternalSigning;
}