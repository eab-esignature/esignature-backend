package com.eab.rest.config;

import org.glassfish.jersey.server.ResourceConfig;
import com.eab.rest.resources.*;

public class JerseyResourcesConfig extends ResourceConfig {
	public JerseyResourcesConfig(){
        register(eSignatureAPI.class);
    }
}
