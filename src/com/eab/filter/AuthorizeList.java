package com.eab.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AuthorizeList {
	public static String heaherName = "x-axa-ease-secret";
	
	public static ArrayList<String> url = new ArrayList<String>();
	public static Map<String, String> authuser = new HashMap<String, String>();
	
	public static void init() {
		//URL Path requires Authentication
		url.add("/ease-api/userprofile/update");
		
		//Known AXA User Account
		authuser.put("UMC", "J0FYQVNHLVVNQy0wMDAwMDAwMCc6J05WenVQaG5CT0cn");
	}
	
	public static boolean match(String path) {
		for (String item : url) {
			if (item.equals(path)) {
				return true;
			}
		}
		return false;
	}
	
	public static String auth(String userCode) {
		for (Map.Entry<String, String> entry : authuser.entrySet()) {
			if (entry.getValue().equals(userCode)) {
				return entry.getKey();
			}
		}
		return null;
	}
}
