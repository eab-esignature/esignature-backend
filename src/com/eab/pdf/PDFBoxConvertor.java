package com.eab.pdf;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.EnvVariable;
import com.eab.pdf.digitalsignature.CreateVisibleSignature;

public class PDFBoxConvertor extends PDFConvertor {
	private Logger logger = Logger.getLogger(PDFBoxConvertor.class);

	public PDFBoxConvertor() throws IOException {
		super();
	}

	@SuppressWarnings("unchecked")
	public boolean pdf2img(String srcFileName, String targetFilePath)
			throws PDFException, PDFSecurityException, IOException, InterruptedException {
		logger.debug("convertPDFtoIMG start");
		boolean success = false;
		try (PDDocument doc = PDDocument.load(new File(srcFileName));) {
			PDFRenderer renderer = new PDFRenderer(doc);
			float imageDPI = 100;
			float pageHeight = 0.0f;
			float pageWidth = 0.0f;
			if (EnvVariable.get("sys.imageDPI") != null && !"".equals(EnvVariable.get("sys.imageDPI")))
				imageDPI = Float.parseFloat(EnvVariable.get("sys.imageDPI"));
			for (int i = 0; i < doc.getNumberOfPages(); i++) {
				PDPage page = doc.getPage(i);
				pageHeight = page.getBBox().getHeight();
				pageWidth = page.getBBox().getWidth();
				BufferedImage image = renderer.renderImageWithDPI(i, imageDPI, ImageType.RGB);
				RenderedImage rendImage = image;
				File file = new File(targetFilePath + this.srcPDFName + (i + 1) + ".jpg");
				ImageIO.write(rendImage, "png", file);
				image.flush();
				image = null;
				rendImage = null;
			}
			this.getConvertResult().setTotPageCnt(doc.getNumberOfPages());
			this.getConvertResult().setPageHeight(pageHeight);
			this.getConvertResult().setPageWidth(pageWidth);
			logger.debug("convertPDFtoIMG end");
			success = true;
		} catch (Exception ex) {
			logger.error(ex.toString());
			success = false;
			throw ex;
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	public boolean pdf2imgAsync(String srcFileName, String targetFilePath)
			throws PDFException, PDFSecurityException, IOException, InterruptedException {
		logger.debug("convertPDFtoIMG Async start");
		boolean success = false;
		PDFRenderer renderer1st = null;
		PDPage page1st = null;
		BufferedImage image1st = null;
		RenderedImage rendImage1st = null;
		try {
			PDDocument doc = PDDocument.load(new File(srcFileName));
			final float imageDPI = EnvVariable.get("sys.imageDPI") != null
					&& !"".equals(EnvVariable.get("sys.imageDPI")) ? Float.parseFloat(EnvVariable.get("sys.imageDPI"))
							: 100;
//			float pageHeight = 0.0f;
//			float pageWidth = 0.0f;
			// convert 1st page start
			renderer1st = new PDFRenderer(doc);
			page1st = doc.getPage(0);
//			pageHeight = page1st.getBBox().getHeight();
//			pageWidth = page1st.getBBox().getWidth();
			this.getConvertResult().setTotPageCnt(doc.getNumberOfPages());
			this.getConvertResult().setPageHeight(page1st.getBBox().getHeight());
			this.getConvertResult().setPageWidth(page1st.getBBox().getWidth());
			image1st = renderer1st.renderImageWithDPI(0, imageDPI, ImageType.RGB);
			rendImage1st = image1st;
			File file1st = new File(targetFilePath + this.srcPDFName + "_1.jpg");
			ImageIO.write(rendImage1st, "png", file1st);
			image1st.flush();
			image1st = null;
			rendImage1st = null;
			renderer1st = null;
			// convert 1st page end

			CompletableFuture<Object>[] futuresList = new CompletableFuture[doc.getNumberOfPages()];
			for (int i = 1; i < doc.getNumberOfPages(); i++) {
//				PDPage page = doc.getPage(i);
//				pageHeight = page.getBBox().getHeight();
//				pageWidth = page.getBBox().getWidth();
				final int j = i;
				final String imageFileName = this.srcPDFName + "_";
				CompletableFuture.supplyAsync(() -> {
					BufferedImage image = null;
					PDFRenderer renderer = null;
					RenderedImage rendImage = null;
					File file = null;
					try {
						renderer = new PDFRenderer(doc);
						image = renderer.renderImageWithDPI(j, imageDPI, ImageType.RGB);
						rendImage = image;
						file = new File(targetFilePath + imageFileName + (j + 1) + ".jpg");
						ImageIO.write(rendImage, "png", file);
						image.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error(e.toString());
					} finally {
						image = null;
						renderer = null;
						rendImage = null;
					}

					return true;
				});
			}
			try {
				CompletableFuture.allOf(futuresList).join();
			} catch (Exception ex) {
				logger.error(ex.toString());
			}

			logger.debug("convertPDFtoIMG Async end");
			success = true;
		} catch (Exception ex) {
			logger.error(ex.toString());
			success = false;
			throw ex;
		} finally {
			renderer1st = null;
			page1st = null;
			image1st = null;
			rendImage1st = null;
		}

		return success;
	}

//	public static void main(String[] args) {
//
//		try {
//			PDFBoxConvertor pPDFBoxConvertor = new PDFBoxConvertor();
////			pPDFBoxConvertor.pdf2img(
////					"C:\\Users\\zeng.fanzhong\\Downloads\\PDF\\102-2198749.QU003088-00354.proposal.pdf",
////					"F:\\pdfconvertion\\convert");
//
////			pPDFBoxConvertor.pdf2imgTest(
////					"C:\\Users\\zeng.fanzhong\\Downloads\\PDF\\102-2198749.QU003088-00354.proposal.pdf",
////					"F:\\pdfconvertion\\convert",0);
//			pPDFBoxConvertor.pdf2imgTest("F:\\pdfconvertion\\demo_chi.pdf", "F:\\pdfconvertion\\convert", 0);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

//	public void pdf2imgTest(String srcFileName, String targetFilePath, int startPage) throws IOException {
//		PDDocument document = PDDocument.load(new File(srcFileName));
//		PDFRenderer pdfRenderer = new PDFRenderer(document);
//
//		int pdfPageCnt = document.getNumberOfPages();
//		// render max 10 page in 1 time
//		int renderPage = pdfPageCnt - startPage < 10 ? pdfPageCnt - startPage : 10;
//
//		CompletableFuture<String>[] futuresList = new CompletableFuture[renderPage];
//		for (int page = 0; page < renderPage; page++) {
//			final int _page = page + startPage;
//			futuresList[page] = CompletableFuture.supplyAsync(() -> renderImg(pdfRenderer, _page, targetFilePath));
//		}
//
//		CompletableFuture.allOf(futuresList).join();
//		String[] base64Imgs = Arrays.stream(futuresList).map(CompletableFuture::join).toArray(String[]::new);
//		document.close();
//	}

//	private String renderImg(PDFRenderer pdfRenderer, int page, String targetFilePath) {
//		try {
//			// convert page to image
//			BufferedImage image = pdfRenderer.renderImageWithDPI(page, 100, ImageType.RGB);
//
//			// convert buffer image to bytes
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			File file = new File(targetFilePath + "\\eSignature_" + (page + 1) + ".jpg");
//			ImageIO.write(image, "png", baos);
//			ImageIO.write(image, "png", file);
//			image.flush();
//			baos.flush();
//
//			byte[] imageInByte = baos.toByteArray();
//			baos.close();
//
//			// output image in base64 format
//			return new String(Base64.encodeBase64(imageInByte), "UTF-8");
//		} catch (Exception e) {
//			return null;
//		}
//	}

//	public void mergeSignToPDF1(String sessionID, String keyWords) throws Exception {
//		String filePath = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin + "\\"
//				+ this.srcPDFName;
//		File file = new File(filePath);
//		PDDocument doc = PDDocument.load(file);
//		PDImageXObject pdImage = PDImageXObject.createFromFile("D:\\pdfconvertion\\timg2.png", doc);
//		PdfBoxKeyWordPosition pdf = new PdfBoxKeyWordPosition(keyWords, filePath);
//		pdf.setAnchor("botton-right");
//		PDPageContentStream contentStream = null;
//		List<float[]> list = pdf.getCoordinate();
//		for (float[] fs : list) {
//			PDPage page = doc.getPage((int) fs[2] - 1);
//			float x = fs[0];
//			float y = fs[1];
//			contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, false);
//			contentStream.drawImage(pdImage, x, y);
////			contentStream.drawImage(pdImage, x, y,pdImage.getWidth(),pdImage.getHeight());
//			contentStream.close();
//		}
//		doc.save(this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin + "\\"
//				+ this.signedPDFName);
//		doc.close();
//
//	}

	@Override
	public boolean mergeSignToPDF(String sessionID, JSONObject jsonObject) throws Exception {
		String filePath = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin + "\\"
				+ jsonObject.getString("docID") + ".pdf";
		String targetPDFPath = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisResultLocatoin;

		return this.mergeImageToPDF(filePath, jsonObject, targetPDFPath);

	}

	public boolean mergeImageToPDF(String filePath, JSONObject jsonObject, String targetPDFPath) throws Exception {
//		String filePath = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin + "\\"
//				+ this.srcPDFName;
		boolean result = false;
		File file = null;
		PDDocument doc = null;
		HashMap<String, PDImageXObject> pdImages = new HashMap<String, PDImageXObject>();
		JSONArray imageArray = null;
		PDPageContentStream contentStream = null;
		try {
			file = new File(filePath);
			doc = PDDocument.load(file);
//		PDImageXObject pdImage = PDImageXObject.createFromFile("f:\\pdfconvertion\\timg2.png", doc);

			imageArray = jsonObject.getJSONArray("imgStore");
			if (imageArray != null && imageArray.length() > 0) {
				for (int i = 0; i < imageArray.length(); i++) {
					JSONObject imagesBase64 = imageArray.getJSONObject(i);
					PDImageXObject signatureImg = PDImageXObject.createFromByteArray(doc,
							Base64.decodeBase64(imagesBase64.getString("data").replace("data:image/png;base64,", "")),
							"PNG");
					pdImages.put(imagesBase64.getString("key"), signatureImg);
				}
			} else {
				throw new IOException("there is no any signature image");
			}
			JSONArray jsonArray = jsonObject.getJSONArray("signatureFields");
			if (jsonArray != null && jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject signatureField = jsonArray.getJSONObject(i);
					int pageIndex = Integer.parseInt(signatureField.getString("pageIndex").substring(0,
							signatureField.getString("pageIndex").indexOf(".")));
					PDPage page = doc.getPage(pageIndex);
					float x = Float.parseFloat(signatureField.getString("x"));
					float y = Float.parseFloat(signatureField.getString("y"));
					contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, false);
					float height = Float.parseFloat(signatureField.getString("height"));
					float width = Float.parseFloat(signatureField.getString("width"));
//					System.out.println("x = " + x + ",y = " + y + ",height = " + height + ",width = " + width);
					contentStream.drawImage(pdImages.get(signatureField.getString("imgKey")), x - width, y - height,
							width, height);
					contentStream.close();
				}
			}
			doc.save(targetPDFPath + "\\" + this.signedPDFName);
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex.toString());
			result = false;
		} finally {
			file = null;
			doc.close();
			doc = null;
			pdImages.clear();
			pdImages = null;
			contentStream = null;
		}
		return result;
	}

	@Override
	public void analyzePDF(String sessionID) throws Exception {
		logger.debug("analyzePDF start");
		// TODO Auto-generated method stub
		String filePath = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin + "\\"
				+ this.srcPDFName + ".pdf";
		JSONArray signRules = this.geteSignatureRule().getJSONArray("signatureRules");
		List<HashMap<String, String>> resultList = convertResult.getResultList() != null
				&& convertResult.getResultList().size() >= 0 ? convertResult.getResultList()
						: new ArrayList<HashMap<String, String>>();
		if (signRules != null && signRules.length() > 0) {
			for (int i = 0; i < signRules.length(); i++) {
				JSONObject signRule = signRules.getJSONObject(i);
				String lookupKey = signRule.getString("lookupKey");
				String signerName = signRule.getString("signerName");
				String signID = signRule.getString("signID");
				String offSet = signRule.getString("offset");
				float height = Float.parseFloat(signRule.getString("height").replaceAll("px", "")) * 0.75f;
				float width = Float.parseFloat(signRule.getString("width").replaceAll("px", "")) * 0.75f;
//				String anchor = signRule.getString("anchor");
				String origin = signRule.getString("origin");
				boolean repeat = true;
				try {
					repeat = signRule.getBoolean("repeat");
				} catch (Exception ex) {

				}
				float x = Float.parseFloat(offSet.split(",")[0]) * 0.75f;
				float y = Float.parseFloat(offSet.split(",")[1]) * 0.75f;

				int seq = 0;
				float tempHeight = height;
				float tempWidth = width;
				if (origin.indexOf("bottom") >= 0) {
					y = y + tempHeight;
				}
				if (origin.indexOf("left") >= 0) {
					x = x + tempWidth;
				}
				List<float[]> wordPosition = this.findKeyWordPosition(lookupKey, filePath);
				if (wordPosition != null && wordPosition.size() > 0) {
					for (int j = 0; j < (repeat?wordPosition.size():1); j++) {
						seq++;
						HashMap<String, String> result = new HashMap<String, String>();
						result.put("lookupkey", lookupKey);
						result.put("signerName", signerName);
						result.put("signID", signID);
						float subX = wordPosition.get(j)[0] + x;
						float subY = wordPosition.get(j)[1] + y;
//						logger.error("x = "+subX+"y = "+subY);
//						logger.error("wordPosition.get(j)[0] = "+wordPosition.get(j)[0]+"wordPosition.get(j)[1] = "+wordPosition.get(j)[1]);
						result.put("x", String.valueOf(subX));
						result.put("y", String.valueOf(subY));
						result.put("pageIndex", String.valueOf(wordPosition.get(j)[2] - 1));
						result.put("height", String.valueOf(height));
						result.put("width", String.valueOf(width));
						result.put("seq", String.valueOf(seq));
						resultList.add(result);
					}
				}
				convertResult.setResultList(resultList);
				convertResult.isSuccess();
			}
		}
		logger.debug("analyzePDF end");
	}

	@Override
	public List<float[]> findKeyWordPosition(String lookupKey, String filePath) throws Exception {
		// TODO Auto-generated method stub
		try {
			PdfBoxKeyWordPosition pdfBoxKeyWordPosition = new PdfBoxKeyWordPosition(lookupKey, filePath);
			List<float[]> wordPosition = pdfBoxKeyWordPosition.getCoordinate();
			return wordPosition;
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
	}

	@Override
	public boolean signPDF(String sessionID, JSONObject jsonObject) throws Exception {
		try {
			String srcFileFolder = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin;
			String input = FileUtils.readFileToString(new File(srcFileFolder+"\\"+PDFUtil.inputParamJsonFileName), "UTF-8");
			JSONObject tmp = new JSONObject(input);	
			this.setSrcPDFName(tmp.getString("docID"));
			tmp = null;
			String srcPDFFile = srcFileFolder + "\\" + this.srcPDFName + ".pdf";
			JSONArray jsonArray = jsonObject.getJSONArray("signatureFields");
			String strPageH = jsonObject.getString("pageHeight");
			int pageH = strPageH.indexOf(".") > 0 ? Integer.parseInt(strPageH.substring(0, strPageH.indexOf(".")))
					: Integer.parseInt(strPageH);
			String imageFolder = fileAnalysisLocation + "\\" + jsonObject.getString("sessionID") + "\\"
					+ this.fileAnalysisResultLocatoin;
			String signedPDFName = this.srcPDFName + "_signed.pdf";
			if (jsonArray != null && jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject signatureField = jsonArray.getJSONObject(i);
					String imageKey = signatureField.getString("imgKey");
					int pageIndex = Integer.parseInt(signatureField.getString("pageIndex").substring(0,
							signatureField.getString("pageIndex").indexOf(".")));
					String strX = signatureField.getString("x");
					String strY = signatureField.getString("y");
					String strW = signatureField.getString("width");
					float fX = Float.parseFloat(strX);
					float fY = Float.parseFloat(strY);
					float fW = Float.parseFloat(strW);
					fX = fX - fW;
					fY = pageH - fY;
					int x = Integer.parseInt(String.valueOf(fX).substring(0, String.valueOf(fX).indexOf(".")));
					int y = Integer.parseInt(String.valueOf(fY).substring(0, String.valueOf(fY).indexOf(".")));
					srcPDFFile = this.sign1PDFPage(srcPDFFile, imageFolder + "\\" + imageKey + ".png", ++pageIndex,
							signedPDFName, sessionID, x, y, -50);// zoom out 50%
				}
			}
			return true;
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
	}

//	public String sign1PDFPage(String srcPDFFile,String imageFile,int page,String signedPDFName,String sessionID) throws Exception{
//		File ksFile = null;
//	    KeyStore keystore = null;
//		try {
//			String ksFileName=this.fileAnalysisLocation + "\\keystore.jks";
//			ksFile = new File(ksFileName);
//		    keystore = KeyStore.getInstance("PKCS12");
//		    String ksPWD = EnvVariable.get("sys.keyStorePWD");
//	
//			char pin[] = ksPWD.toCharArray();
//			keystore.load(new FileInputStream(ksFile), pin);
//	       
//			return this.sign1PDFPage(srcPDFFile, imageFile, page, signedPDFName, sessionID,0,0,100,keystore, ksPWD);
//		}catch(Exception ex) {
//			logger.error(ex.toString());
//        	throw ex;
//		}finally{
//			ksFile = null;
//			keystore = null;
//		}
//	}
	public String sign1PDFPage(String srcPDFFile, String imageFile, int page, String signedPDFName, String sessionID,
			int x, int y, int zoomPercent) throws Exception {
		File ksFile = null;
		KeyStore keystore = null;
		try {
			String ksFileName = EnvVariable.get("sys.keyStoreFilePath") + "\\"
					+ EnvVariable.get("sys.keyStoreFileName");
			ksFile = new File(ksFileName);
			keystore = KeyStore.getInstance("PKCS12");
			String ksPWD = EnvVariable.get("sys.keyStorePWD");
			keystore.load(new FileInputStream(ksFile), ksPWD.toCharArray());

			return this.sign1PDFPage(srcPDFFile, imageFile, page, signedPDFName, sessionID, x, y, zoomPercent, keystore,
					ksPWD);
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		} finally {
			ksFile = null;
			keystore = null;
		}
	}

	public String sign1PDFPage(String srcPDFFile, String imageFile, int pageIndex, String signedPDFName,
			String sessionID, int x, int y, int zoomPercent, KeyStore keystore, String ksPWD) throws Exception {
		FileOutputStream fos = null;
		InputStream input = null;
		try {
			String signName = EnvVariable.get("sys.eSignatureName");
			String signLocation = EnvVariable.get("sys.eSignatureLocation");
			String signReason = EnvVariable.get("sys.eSignatureReason");
			char pin[] = ksPWD.toCharArray();
			File documentFile = new File(srcPDFFile); // myTest
			CreateVisibleSignature signing = new CreateVisibleSignature(keystore, (char[]) pin.clone());
			InputStream imageStream = new FileInputStream(imageFile); // myTest
			File signedDocumentFileTmp = File.createTempFile("eSignature", ".png");
			signing.setVisibleSignDesigner(srcPDFFile, x, y, zoomPercent, imageStream, pageIndex);
			imageStream.close();
			signing.setVisibleSignatureProperties(signName, signLocation, signReason, 0, pageIndex, true);
			signing.setExternalSigning(false);
			signing.signPDF(documentFile, signedDocumentFileTmp, null);
			String signedPDFFileName = documentFile.getParent() + "\\";
			if(this.srcPDFName.indexOf(".")>=0)
				signedPDFFileName = signedPDFFileName + this.srcPDFName.substring(0, this.srcPDFName.indexOf(".")) + "_signed.pdf";
			else
				signedPDFFileName = signedPDFFileName + this.srcPDFName + "_signed.pdf";
			File signedDocumentFile = new File(signedPDFFileName);
			input = new FileInputStream(signedDocumentFileTmp);
			fos = new FileOutputStream(signedDocumentFile);
			byte[] buff = new byte[1024];
			int b;
			while ((b = input.read(buff)) != -1) {
				fos.write(buff, 0, b);
			}
			fos.flush();
			signedDocumentFileTmp = null;
			signedDocumentFile = null;
			return signedPDFFileName;
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		} finally {
			fos.close();
			input.close();
			fos = null;
			input = null;
		}
	}

	public void analyzePDFAsync(String sessionID) throws Exception {
		this.analyzePDF(sessionID);
	}
}
