package com.eab.rest.resources;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.UserConfigVariable;
import com.eab.common.eSignSessionContext;
import com.eab.image.ImageUtil;
import com.eab.pdf.PDFConvertResult;
import com.eab.pdf.PDFUtil;

@Path("/")
public class eSignatureAPI {
	private Logger logger = Logger.getLogger(eSignatureAPI.class);

	@POST
	@Path("/session_request")
	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	public String analysePDF(@Context HttpServletRequest req, String data) {
		HttpSession session = req.getSession(true);
		String sessionID = session.getId();
		JSONObject errors = new JSONObject();
		HashMap<String, String> error = null;
		List<HashMap<String, String>> validateResult = new ArrayList<HashMap<String, String>>();
		int totPageCnt = 0;
		float pageHeight = 0.0f;
		float pageWidth = 0.0f;

		try {
			JSONObject jsonObject = new JSONObject(data);
			if ("true".equals(EnvVariable.get("sys.allowCustomize"))) {
				errors = new JSONObject(data);
				errors.remove("binary");
			}
			logger.info(req.getRemoteAddr() + "," + sessionID + ",session_request");
			validateResult = Function.validateInputJson(jsonObject, 1);
			if (validateResult.size() <= 0) {
				PDFUtil pDFBoxUtil = new PDFUtil();
				PDFConvertResult proceedResult = pDFBoxUtil.proceed(sessionID, jsonObject);
				totPageCnt = proceedResult.getTotPageCnt();
				pageHeight = proceedResult.getPageHeight();
				pageWidth = proceedResult.getPageWidth();
				errors.put("signatureFields", proceedResult.getResultList());
				if (!proceedResult.isSuccess())
					validateResult.add(proceedResult.getErrors());
			}
		} catch (org.json.JSONException ex) {
			logger.error(ex.toString());
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "004");
			error.put("subCode", "001");
			error.put("reasonTXT", "Incorrect json format");
			error.put("subReasonTXT", "Incorrect json format");
			validateResult.add(error);
			logger.debug("004-001-The json format is incorrect.");
		} catch (Exception e) {
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "001");
			error.put("reasonTXT", "Server error");
			error.put("subReasonTXT", "Server error");
			validateResult.add(error);
			logger.error(e.toString());
		}
		if (validateResult != null && validateResult.size() > 0) {
			errors.put("Status", "Fail");
			errors.put("sessionID", sessionID);
			errors.put("desc", "Description");
			errors.put("exception", validateResult);
		} else {
			errors.put("Status", "Success");
			errors.put("sessionID", sessionID);
			errors.put("desc", "Description");
			errors.put("pageCount", String.valueOf(totPageCnt));
			errors.put("pageHeight", String.valueOf(pageHeight) + "pt");
			errors.put("pageWidth", String.valueOf(pageWidth) + "pt");
		}
		logger.debug(errors.toString());
		return errors.toString();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/loadImage")
	public String loadImage(@Context HttpServletRequest req, String data) {
		String imageBase64 = null;
		JSONObject jsonObject = null;
		HashMap<String, String> error = null;
		List<HashMap<String, String>> validateResult = new ArrayList<HashMap<String, String>>();
		String sessionID = null;
		try {
			jsonObject = new JSONObject(data);
			validateResult = Function.validateInputJson(jsonObject, 2);
			if (validateResult.size() <= 0) {
				sessionID = jsonObject.getString("sessionID");

				HttpSession session = eSignSessionContext.getSession(sessionID);
				if (session == null) {
					error = new HashMap<String, String>();
					error.put("errorType", "ERROR");
					error.put("reasonCD", "003");
					error.put("subCode", "002");
					error.put("reasonTXT", "Server error");
					error.put("subReasonTXT", "sessionID:" + sessionID + " expired or session does not exists");
					validateResult.add(error);
					logger.error("sessionID:" + sessionID + " expired or session does not exists");
				} else {
					ImageUtil imageUtil = new ImageUtil();
					logger.info(req.getRemoteAddr() + "," + sessionID + ",loadImage");
					imageBase64 = imageUtil.localImageToBASE64(sessionID, jsonObject.getString("pageIndex"));
					jsonObject.put("Status", "Success");
					jsonObject.put("data", imageBase64);
				}
			}
		} catch (IOException e) {
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "003");
			error.put("reasonTXT", "Server error");
			error.put("subReasonTXT", "Server error");
			validateResult.add(error);
			logger.error(e.toString());
		} catch (Exception e) {
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "001");
			error.put("reasonTXT", "Server error");
			error.put("subReasonTXT", "Server error");
			validateResult.add(error);
			logger.error(e.toString());
		}
		if (validateResult != null && validateResult.size() > 0) {
			jsonObject.put("Status", "Fail");
			jsonObject.put("desc", "Description");
			jsonObject.put("exception", validateResult);
		} else {
			jsonObject.put("Status", "Success");
			jsonObject.put("desc", "Description");
		}
		return jsonObject.toString();
	}

//	@GET
//	@Produces(MediaType.TEXT_PLAIN)
//	@Path("/fileToBase64")
//	public String fileToBase64(String filePath) {
//		String base64 = null;
//		try {
//			JSONObject jsonObject = new JSONObject(filePath);
//			ImageUtil imageUtil = new ImageUtil();
//			base64 = imageUtil.image2BASE64(jsonObject.getString("filePath"));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return base64;
//	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/saveSignature")
	public String saveSignature(@Context HttpServletRequest req, String data) {
		JSONObject jsonObject = new JSONObject();
		List<HashMap<String, String>> validateResult = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> error = null;
		try {
			JSONObject param = new JSONObject(data);
			validateResult = Function.validateInputJson(param, 3);
			if (validateResult.size() <= 0) {

				String sessionID = param.getString("sessionID");
				HttpSession session = eSignSessionContext.getSession(sessionID);
				if (session == null) {
					error = new HashMap<String, String>();
					error.put("errorType", "ERROR");
					error.put("reasonCD", "003");
					error.put("subCode", "002");
					error.put("reasonTXT", "Server error");
					error.put("subReasonTXT", "sessionID:" + sessionID + " expired or session does not exists");
					validateResult.add(error);
					logger.error("sessionID:" + sessionID + " expired or session does not exists");
				} else {
					PDFUtil pDFBoxUtil = new PDFUtil();
					if (!pDFBoxUtil.saveSignature(sessionID, param))
						throw new IOException("Save Signature Failed");
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "001");
			error.put("reasonTXT", "Server error");
			error.put("subReasonTXT", "Server error");
			validateResult.add(error);
		}

		if (validateResult != null && validateResult.size() > 0) {
			jsonObject.put("Status", "Fail");
			jsonObject.put("desc", "Description");
			jsonObject.put("exception", validateResult);
		} else {
			jsonObject.put("Status", "Success");
			jsonObject.put("desc", "Description");
		}
		return jsonObject.toString();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/loadProcessResult")
	public String loadData(@Context HttpServletRequest req, String data) {
		JSONObject jsonObject = new JSONObject();
		List<HashMap<String, String>> validateResult = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> error = null;
		try {
			JSONObject param = new JSONObject(data);
			validateResult = Function.validateInputJson(param, 4);
			if (validateResult.size() <= 0) {
				String sessionID = param.getString("sessionID");
				HttpSession session = eSignSessionContext.getSession(sessionID);
				if (session == null) {
					error = new HashMap<String, String>();
					error.put("errorType", "ERROR");
					error.put("reasonCD", "003");
					error.put("subCode", "002");
					error.put("reasonTXT", "Server error");
					error.put("subReasonTXT", "sessionID:" + sessionID + " expired or session does not exists");
					validateResult.add(error);
					logger.error("sessionID:" + sessionID + " expired or session does not exists");
				} else {
					PDFUtil pDFBoxUtil = new PDFUtil();
					jsonObject = pDFBoxUtil.loadAnalyzeResult(sessionID);
					Map<String, String> userConfig = UserConfigVariable.getDefaultVars();
					jsonObject.put("config", userConfig);
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "001");
			error.put("reasonTXT", "Server error");
			error.put("subReasonTXT", "Server error");
			validateResult.add(error);
		}

		if (validateResult != null && validateResult.size() > 0) {
			jsonObject = new JSONObject();
			jsonObject.put("Status", "Fail");
			jsonObject.put("desc", "Description");
			jsonObject.put("exception", validateResult);
		} else {
			jsonObject.put("Status", "Success");
			jsonObject.put("desc", "Description");
		}
		return jsonObject.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/loadSignedPDF")
	public String loadSignedPDF(@Context HttpServletResponse resp, @Context HttpServletRequest req) {
		HashMap<String, String> error = null;
		JSONObject jsonObject = new JSONObject();
		List<HashMap<String, String>> validateResult = new ArrayList<HashMap<String, String>>();
		try {
			ByteArrayOutputStream outstrem = null;
//			validateResult = Function.validateInputJson(jsonObject, 4);
			if (validateResult != null && validateResult.size() <= 0) {

//				String sessionID = jsonObject.getString("sessionID");
//				String sessionID = filePath;
				String sessionID = req.getParameter("sessionID");
				HttpSession session = eSignSessionContext.getSession(sessionID);
				if (session == null) {
					error = new HashMap<String, String>();
					error.put("errorType", "ERROR");
					error.put("reasonCD", "003");
					error.put("subCode", "002");
					error.put("reasonTXT", "Server error");
					error.put("subReasonTXT", "sessionID:" + sessionID + " expired or session does not exists");
					validateResult.add(error);
					logger.error("sessionID:" + sessionID + " expired or session does not exists");
				} else {
					PDFUtil pDFBoxUtil = new PDFUtil();
					if ("BASE64".equals(UserConfigVariable.get("user.signedPDFOutputType"))) {
						jsonObject.put("data", (String) pDFBoxUtil.loadSignedPDF(sessionID));
					} else if ("IO_STREAM".equals(UserConfigVariable.get("user.signedPDFOutputType"))) {
						resp.setContentType("application/pdf");
						outstrem = (ByteArrayOutputStream) pDFBoxUtil.loadSignedPDF(sessionID);
						resp.getOutputStream().write(outstrem.toByteArray());
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			error = new HashMap<String, String>();
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "001");
			error.put("reasonTXT", "Server error");
			error.put("subReasonTXT", "Server error");
			validateResult.add(error);
		}
		if (validateResult != null && validateResult.size() > 0) {
			jsonObject = new JSONObject();
			jsonObject.put("Status", "Fail");
			jsonObject.put("desc", "Description");
			jsonObject.put("exception", validateResult);
			return jsonObject.toString();
		}
		return null;

	}
}
