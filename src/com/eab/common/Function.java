package com.eab.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Date;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.pdf.PDFUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Calendar;

public class Function {
	private static Logger logger = Logger.getLogger(PDFUtil.class);

	public static void changeTimezone(Calendar cal, String timezone) {
		cal.setTimeZone(TimeZone.getTimeZone(timezone));
	}

	public static Object jsonObject2Object(JSONObject jo, String str) {
		if (jo != null) {
			String[] strAr = str.split("\\.");
			for (int i = 0; i < strAr.length; i++) {
				String key = strAr[i];
				if (jo.has(key)) {
					if (jo.get(key) instanceof JSONObject)
						jo = jo.getJSONObject(key);
					else if (jo.get(key) instanceof Object)
						return jo.get(key);
				} else
					break;
			}
		}
		return null;
	}

	public static String covertOracleDateToISO8601(String dateStr) {
		try {
			DateTimeFormatter inputParser = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime localDateTime = LocalDateTime.parse(dateStr, inputParser);
			DateTimeFormatter outputPrinter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			return outputPrinter.format(localDateTime);
		} catch (Exception e) {
			return "";
		}
	}

	public static Calendar convertISO8601(long timestamp) {
		ZonedDateTime utc = Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC);
		return GregorianCalendar.from(utc);
	}

//    public static Calendar convertISO8601(String dateStr) {
//        return javax.xml.bind.DatatypeConverter.parseDateTime(dateStr);
//    }

	public static String cal2StrFull(Calendar cal) {
		return cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE) + " "
				+ cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + ":"
				+ cal.get(Calendar.MILLISECOND) + " " + cal.getTimeZone().getDisplayName();
	}

	public static String cal2StrDate(Calendar cal) {

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);

		return year + "-" + (month < 10 ? "0" : "") + month + "-" + (date < 10 ? "0" : "") + date;
	}

	public static String jsonObject2String(JSONObject jo, String str) {
		if (jo != null) {
			String[] strAr = str.split("\\.");

			for (int i = 0; i < strAr.length; i++) {
				String key = strAr[i];
				if (jo.has(key)) {
					if (jo.get(key) instanceof JSONObject)
						jo = jo.getJSONObject(key);
					else if (jo.get(key) instanceof String)
						return jo.getString(key);
				} else
					break;
			}
		}
		return "";
	}

	public static String cal2Str(Calendar cal) {

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);

		return year + "-" + (month < 10 ? "0" : "") + month + "-" + (date < 10 ? "0" : "") + date + " "
				+ (hour < 10 ? "0" : "") + hour + ":" + (min < 10 ? "0" : "") + min;
	}

	public static String GenerateRandomString(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return new Base64().encodeAsString(bytes);
	}

	public static String GenerateRandomASCII(int length) {
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		// Random rng = new Random();
		java.security.SecureRandom rng = new java.security.SecureRandom();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	public static String ByteToStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		for (byte b : bytes)
			sb.append((char) b);
		return sb.toString();
	}

	public static byte[] StrToByte(String text) {
		return text.getBytes();
	}

	public static String ByteToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static byte[] HexToByte(String s) {
		return new BigInteger(s, 16).toByteArray();
	}

	public static byte[] StrToBase64(String text) {
		return text.getBytes();
	}

	public static String Base64ToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static String ByteToBase64(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}

	public static byte[] HexToStr(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
		}
		return data;
	}

	private static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static String StrToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String getClientIp(HttpServletRequest request) {
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}
		return remoteAddr;
	}

	public static String getServerIp() {
		String ipAddr = "0.0.0.0";

		try {
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
				Enumeration<InetAddress> nias = ni.getInetAddresses();
				while (nias.hasMoreElements()) {
					InetAddress ia = (InetAddress) nias.nextElement();
					if (!ia.isLinkLocalAddress() && !ia.isLoopbackAddress() && ia instanceof Inet4Address) {
						return ia.getHostAddress();
					}
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return ipAddr;
	}

	public static String getServerIp(HttpServletRequest request) {
		String ipAddr = (request != null) ? request.getLocalAddr() : "0.0.0.0";
		return ipAddr;
	}

	public static String getBaseUrl(HttpServletRequest request) {
		return (request.getRequestURL()).substring(0, (request.getRequestURL()).length()
				- (request.getRequestURI()).length() + (request.getContextPath()).length()) + "/";
	}

	public static String getPathUrl(HttpServletRequest request) {
		return request.getServletPath();
	}

	public static ZonedDateTime getDateUTCNow() {
		return ZonedDateTime.now(ZoneOffset.UTC);
	}

	public static java.sql.Date covertZonedDateTimeToSqlDate(ZonedDateTime zDate) {
		return new java.sql.Date(java.util.Date.from(zDate.toInstant()).getTime());
	}

	public static boolean dateFormatValidator(String value, String format) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			dateFormat.parse(value);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static String convertDateString(String value, String fromFormat, String toFormat) throws ParseException {

		String convertedDateString = "";

		SimpleDateFormat fromDateFormatter = new SimpleDateFormat(fromFormat);

		java.util.Date date = fromDateFormatter.parse(value);

		SimpleDateFormat toDateFormatter = new SimpleDateFormat(toFormat);

		convertedDateString = toDateFormatter.format(date);

		return convertedDateString;
	}

	public static JSONObject merge2JSONObject(JSONObject json1, JSONObject json2) {
		JSONObject mergedJSON = new JSONObject();
		try {
			mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
			for (String crunchifyKey : JSONObject.getNames(json2)) {
				mergedJSON.put(crunchifyKey, json2.get(crunchifyKey));
			}

		} catch (Exception e) {
			logger.error(e);
		}
		return mergedJSON;
	}

	public static JsonObject validateJsonFormatForString(String jsonString) {
		JsonObject jsonBody = null;

		try {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
			jsonBody = jsonElement.getAsJsonObject();

		} catch (Exception ex) {
			logger.error(ex);
		}

		return jsonBody;
	}

	public static JsonArray validateJsonArrayFormatForString(String jsonString) {
		JsonArray jsonBody = null;

		try {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
			jsonBody = jsonElement.getAsJsonArray();

		} catch (Exception ex) {
			logger.error(ex);
		}

		return jsonBody;
	}

	public static String extractException(Exception e) {
		String message = e.toString() + "\n";
		StackTraceElement[] trace = e.getStackTrace();
		for (int i = 0; i < trace.length; i++) {
			message += trace[i].toString() + "\n";
		}

		return message;
	}

	public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	public static java.sql.Date sqlDateFromString(String value, String dateFormat, String targetTimeZoneId)
			throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setTimeZone(TimeZone.getTimeZone(targetTimeZoneId));
		java.util.Date parsed = format.parse(value);

		return convertJavaDateToSqlDate(parsed);
	}

	public static boolean isNumeric(String text) {
		boolean result = false;
		try {
			Integer.parseInt(text);
			result = true;
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	public static boolean isASCII(String text) {
		boolean result = false;
		if (text != null)
			result = text.matches("\\p{ASCII}*");

		return result;
	}

	public boolean transformEleToBoolean(JsonElement ele) {
		if (ele == null) {
			return false;
		} else {
			return ele.getAsBoolean();
		}
	}

	public static String getServerName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.error(e);
		}
		return "";
	}

	public static JsonElement getAsPath(JsonElement e, String path) {
		JsonElement current = e;
		String ss[] = path.split("/");
		for (int i = 0; i < ss.length; i++) {
			if (current != null) {
				current = current.getAsJsonObject().get(ss[i]);
			}
		}
		return current;
	}

	public JsonArray transformEleToArray(JsonElement ele) {
		if (ele == null) {
			return new JsonArray();
		} else {
			return ele.getAsJsonArray();
		}
	}

	public JsonObject transformEleToObj(JsonElement ele) {
		if (ele == null) {
			return new JsonObject();
		} else {
			return ele.getAsJsonObject();
		}
	}

	public static JsonObject transformObject(JSONObject obj) {
		if (obj == null) {
			return null;
		}
		JsonParser jsonParser = new JsonParser();
		JsonObject transformedObj;
		transformedObj = (JsonObject) jsonParser.parse(obj.toString());
		return transformedObj;
	}

	public static double transformEleToDouble(JsonElement ele) {
		if (ele == null) {
			return 0;
		} else {
			return ele.getAsDouble();
		}
	}

	public static String transformEleToString(JsonElement ele) {
		if (ele == null) {
			return "";
		} else {
			return ele.getAsString();
		}
	}

	public String callgetApi(String path) throws Exception {
		HttpClient httpclient = HttpClients.createDefault();

		HttpGet httpget = new HttpGet(path);
		httpget.setHeader("Content-Type", "application/json");

		// Execute and get the response.
		HttpResponse response = httpclient.execute(httpget);

		// response.getStatusLine().getStatusCode();

		HttpEntity entity = response.getEntity();
		String responseString = "";
		if (entity != null) {
			responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
		}

		return responseString;
	}

	public String callpostApi(JsonObject wfiParams, String path) throws Exception {
		int timeout = Integer.parseInt(EnvVariable.get("WEB_API_WFI_TIMEOUT"));
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout * 1000)
				.setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
		HttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();

//		HttpClient httpclient = HttpClients.createDefault();

		HttpPost httppost = new HttpPost(path);
		httppost.setHeader("Content-Type", "application/json");
		httppost.setEntity(new StringEntity(wfiParams.toString(), StandardCharsets.UTF_8.name()));

		// Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);

		HttpEntity entity = response.getEntity();

		String responseString = "";
		if (entity != null) {
			responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
		}
		httppost.releaseConnection();
		return responseString;
	}

	public JsonObject handleResponse(String resp, String respObjkey) throws Exception {
		logger.debug("Start to handle Response");

		JsonParser jsonParser = new JsonParser();
		JsonObject respObj = (JsonObject) jsonParser.parse(resp);
		JsonObject exceptObj;
		String exceptMsg;
		String exceptReason;
		String exceptCode;
		String successFlag;
		JsonObject response = new JsonObject();
		JsonObject successObj = respObj.getAsJsonObject(respObjkey);
		if (respObj.entrySet().size() == 0) {
			logger.debug("Response Object is Empty, Job Success");
			response.addProperty("success", true);
			response.addProperty("message", "");
			return response;
		} else if (successObj != null) {
			successFlag = getStringFromJsonObject(successObj.get("responseFlag"));
			logger.debug("Print Success Obj");
			logger.debug(successFlag);
			response.addProperty("success", true);
			response.addProperty("message", "");
			return response;
		} else {
			exceptObj = respObj.getAsJsonObject("exception");
			if (exceptObj != null) {
				exceptObj.addProperty("success", false);
				exceptMsg = getStringFromJsonObject(exceptObj.get("message"));
				exceptReason = getStringFromJsonObject(exceptObj.get("reason"));
				exceptCode = getStringFromJsonObject(exceptObj.get("reasonCode"));
				logger.debug("Print Failure obj");
				logger.debug(exceptCode);
				logger.debug(exceptReason);
				logger.debug(exceptMsg);
				response = exceptObj;
			} else {
				response.addProperty("success", false);
				response.addProperty("message", "");
			}

			return response;
		}
	}

	public ArrayList<String> getContentToSend(ArrayList<String> base64Strs, ArrayList<String> fileType)
			throws Exception {
		// ByteArrayOutputStream os = new ByteArrayOutputStream();
		ArrayList<String> contentTosend = new ArrayList<String>();
		for (int i = 0; i < base64Strs.size(); i++) {
			if (fileType.get(i).contains("image")) {
				contentTosend.add(convertImageToPdfBase64Str(base64Strs.get(i), i));
			} else if (fileType.get(i).contains("pdf")) {
				contentTosend.add(base64Strs.get(i));
			}
		}

		return contentTosend;
	}

	private String convertImageToPdfBase64Str(String imgSrc, int index) throws Exception {
		ByteArrayOutputStream byteArrayOutputStream = null;
		byte[] encodedByte;
		InputStream is;
		PDDocument document = null;
		PDPageContentStream contentStream = null;
		String base64Str = "";
		try {
			byteArrayOutputStream = new ByteArrayOutputStream();
			encodedByte = Base64.decodeBase64(imgSrc);
			is = new ByteArrayInputStream(encodedByte);

			document = new PDDocument();
			PDPage pageAdded = new PDPage();
			document.addPage(pageAdded);
			PDPage page = document.getPage(0);
			BufferedImage pdImage = ImageIO.read(is);
			PDImageXObject obj = JPEGFactory.createFromImage(document, pdImage);

			contentStream = new PDPageContentStream(document, page);

			float scale = getImgScale(page, obj, 40);
			contentStream.drawImage(obj, 40f, page.getMediaBox().getHeight() - obj.getHeight() * scale - 40,
					obj.getWidth() * scale, obj.getHeight() * scale);
			contentStream.close();

			document.save(byteArrayOutputStream);
			base64Str = new String(Base64.encodeBase64(byteArrayOutputStream.toByteArray()));
		} finally {
			if (document != null)
				document.close();
			if (byteArrayOutputStream != null)
				byteArrayOutputStream.close();
		}

		return base64Str;

	}

	private float getImgScale(PDPage page, PDImageXObject img, int margin) {
		float scale = 1;
		float availableWidth = page.getMediaBox().getWidth() - margin * 2;
		if (img.getWidth() > availableWidth) {
			scale = Math.min(scale, availableWidth / img.getWidth());
		}
		float availableHeight = page.getMediaBox().getHeight() - margin * 2;
		if (img.getHeight() > availableHeight) {
			scale = Math.min(scale, availableHeight / img.getHeight());
		}
		return scale;
	}

	public String getStringFromJsonObject(JsonElement ele) {
		if (ele != null) {
			return ele.getAsString();
		} else
			return "";
	}

	private static String renderImg(PDFRenderer pdfRenderer, int page) {
		try {
			// convert page to image
			BufferedImage image = pdfRenderer.renderImageWithDPI(page, 100, ImageType.RGB);

			// convert buffer image to bytes
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			baos.flush();

			byte[] imageInByte = baos.toByteArray();
			baos.close();

			// output image in base64 format
			return new String(Base64.encodeBase64(imageInByte), "UTF-8");
		} catch (Exception e) {
			return null;
		}
	}

	private static byte[] convertBase64PDF2Byte(String pdfBase64) throws Exception {
		return Base64.decodeBase64(pdfBase64);
	}

	public static String convertPdf2Img(String pdfBase64, int startPage) {
		try {
			System.out.print("here");
			byte[] pdfByte = convertBase64PDF2Byte(pdfBase64);
			PDDocument document = PDDocument.load(pdfByte);
			PDFRenderer pdfRenderer = new PDFRenderer(document);

			int pdfPageCnt = document.getNumberOfPages();
			// render max 10 page in 1 time
			int renderPage = pdfPageCnt - startPage < 10 ? pdfPageCnt - startPage : 10;

			CompletableFuture<String>[] futuresList = new CompletableFuture[renderPage];
			for (int page = 0; page < renderPage; page++) {
				final int _page = page + startPage;
				futuresList[page] = CompletableFuture.supplyAsync(() -> renderImg(pdfRenderer, _page));
			}

			CompletableFuture.allOf(futuresList).join();
			String[] base64Imgs = Arrays.stream(futuresList).map(CompletableFuture::join).toArray(String[]::new);
			document.close();
			JsonObject result = new JsonObject();
			result.addProperty("isComplete", startPage + renderPage == pdfPageCnt ? true : false);
			result.addProperty("startPage", startPage);
			JsonArray images = new JsonArray();
			for (int i = 0; i < base64Imgs.length; i++) {
				images.add(base64Imgs[i]);
			}
			result.add("data", images);

			return result.toString();

		} catch (Exception e) {
			return null;
		}
	}

	// return true if targetDate before DateToBeCompare, false otherwise
	// targetDate usually today
//	public static boolean dateCompare(String targetDate, String DateToBeCompare) {
//		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
//		Date newtargetDate = new Date();
//		
//		Boolean result = true;
//		try {
//			if(!targetDate.toLowerCase().equals("today")) {
//				newtargetDate = formatter.parse(targetDate);
//			}
//			
//			Date dtbc = formatter.parse(DateToBeCompare);
//			if(newtargetDate.before(dtbc)) {
//				result = true;
//			} else {
//				result = false;
//			}
//		} catch (ParseException e) {
//			Log.error(e);;
//		}
//		
//		return result;
//	}

	// before = today before startDate
	// between = today between startDate and endDate
	// after = today after startDate
//	public static String dateCompare(String startDateString, String endDateString) {
//		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
//		Date today = new Date();
//		
//		String result = "";
//		try {
//			Date startDate = formatter.parse(startDateString);
//			Date endDate = formatter.parse(endDateString);
//			if(today.before(startDate)) {
//				result = "before";
//			} else if(today.after(startDate) && today.before(endDate)) {
//				result = "between";
//			} else if(today.after(endDate)) {
//				result = "after";
//			}
//		} catch (ParseException e) {
//			Log.error(e);;
//		}
//		
//		return result;
//	}

	public static String convertExceptionToClob(Exception e) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString();
			return sStackTrace;
		} catch (Exception exp) {
			logger.error(exp);
			return exp.getMessage();
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e1) {
					logger.error(e1);
				}
			}

			if (pw != null) {
				pw.flush();
				pw.close();
			}
		}

	}

	/*
	 * #
	 */
	/**
	 * @param inputJson    JSONObject of parameter
	 * @param validateType integer 1:base64 data of PDF,signature rules 2:get
	 *                     image/s 3.saveSignature
	 * @return list of error code/error sub code/error message
	 */
	public static List<HashMap<String, String>> validateInputJson(JSONObject inputJson, int validateType) {
		List<HashMap<String, String>> errors = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> error = null;
		if (validateType == 1) {
			try {
				inputJson.get("docID");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "001");
				error.put("reasonTXT", "Incorrect json format");
				error.put("subReasonTXT", "missing signature parameter:docID");
				logger.debug("007-001-missing signature parameter:docID");
				errors.add(error);
			}
			try {
				inputJson.get("binary");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "002");
				error.put("reasonTXT", "Incorrect json format");
				logger.error("007-002-missing signature parameter:binary");
				error.put("subReasonTXT", "missing signature parameter:binary");
				errors.add(error);
			}
			try {
				inputJson.get("localization");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "003");
				error.put("reasonTXT", "Incorrect json format");
				logger.error("007-003-missing signature parameter:localization");
				error.put("subReasonTXT", "missing signature parameter:localization,default localization setting will be apply");
				errors.add(error);
			}
			try {
				JSONArray signatureRules = inputJson.getJSONArray("signatureRules");

				for (int i = 0; i < signatureRules.length(); i++) {
					JSONObject signatureRule = signatureRules.getJSONObject(i);
					try {
						signatureRule.get("lookupKey");
					} catch (org.json.JSONException ex) {
						error = new HashMap<String, String>();
						error.put("errorType", "ERROR");
						error.put("reasonCD", "005");
						error.put("subCode", "001");
						error.put("reasonTXT", "Lookup key not found");
						logger.error("005-001-missing signature parameter:signatureRules.lookupKey");
						error.put("subReasonTXT", "missing signature parameter:signatureRules.lookupKey");
						errors.add(error);
					}
					try {
						signatureRule.get("signerName");
					} catch (org.json.JSONException ex) {
						error = new HashMap<String, String>();
						error.put("errorType", "ERROR");
						error.put("reasonCD", "005");
						error.put("subCode", "017");
						error.put("reasonTXT", "Lookup key not found");
						logger.error("005-017-missing signature parameter:signatureRules.signerName");
						error.put("subReasonTXT", "missing signature parameter:signatureRules.signerName");
						errors.add(error);
					}
					try {
						String height = signatureRule.get("height").toString();
						if (height.indexOf("px") < 0) {
							error = new HashMap<String, String>();
							error.put("errorType", "ERROR");
							error.put("reasonCD", "005");
							error.put("subCode", "002");
							error.put("reasonTXT", "Signature rules error");
							logger.error("005-002-signature parameter:signatureRules.height is not in expected format");
							error.put("subReasonTXT", "signature parameter:signatureRules.height is not in expected format");
							errors.add(error);
						} else {
							try {
								if(height.length()-height.indexOf("px")!=2) {
									error = new HashMap<String, String>();
									error.put("errorType", "ERROR");
									error.put("reasonCD", "005");
									error.put("subCode", "015");
									error.put("reasonTXT", "Signature rules error");
									logger.error("005-015-signature parameter:signatureRules.height is not in expected format");
									error.put("subReasonTXT", "signature parameter:signatureRules.height is not in expected format");
									errors.add(error);									
								}else
								Integer.parseInt(height.replace("px", ""));
							} catch (NumberFormatException ex) {
								error = new HashMap<String, String>();
								error.put("errorType", "ERROR");
								error.put("reasonCD", "005");
								error.put("subCode", "003");
								error.put("reasonTXT", "Signature rules error");
								logger.error("005-003-signature parameter:signatureRules.height is not in expected format");
								error.put("subReasonTXT", "signature parameter:signatureRules.height is not in expected format");
								errors.add(error);
							}
						}
					} catch (org.json.JSONException ex) {
						error = new HashMap<String, String>();
						error.put("errorType", "ERROR");
						error.put("reasonCD", "005");
						error.put("subCode", "004");
						error.put("reasonTXT", "Signature rules error");
						logger.error("005-004-missing signature parameter:signatureRules.height");
						error.put("subReasonTXT", "missing signature parameter:signatureRules.height");
						errors.add(error);
					}
					try {
						String width = signatureRule.get("width").toString();
						if (width.indexOf("px") < 0) {
							error = new HashMap<String, String>();
							error.put("errorType", "ERROR");
							error.put("reasonCD", "005");
							error.put("subCode", "005");
							error.put("reasonTXT", "Signature rules error");
							logger.error("005-005-signature parameter:signatureRules.width is not in expected format");
							error.put("subReasonTXT", "signature parameter:signatureRules.width is not in expected format");
							errors.add(error);
						} else {
							try {
								if(width.length()-width.indexOf("px")!=2) {
									error = new HashMap<String, String>();
									error.put("errorType", "ERROR");
									error.put("reasonCD", "005");
									error.put("subCode", "016");
									error.put("reasonTXT", "Signature rules error");
									logger.error("005-016-signature parameter:signatureRules.width is not in expected format");
									error.put("subReasonTXT", "signature parameter:signatureRules.width is not in expected format");
									errors.add(error);									
								}else
								Integer.parseInt(width.replace("px", ""));
							} catch (NumberFormatException ex) {
								error = new HashMap<String, String>();
								error.put("errorType", "ERROR");
								error.put("reasonCD", "005");
								error.put("subCode", "006");
								error.put("reasonTXT", "Signature rules error");
								logger.error("005-006-signature parameter:signatureRules.width is not in expected format");
								error.put("subReasonTXT", "signature parameter:signatureRules.width is not in expected format");
								errors.add(error);
							}
						}
					} catch (org.json.JSONException ex) {
						error = new HashMap<String, String>();
						error.put("errorType", "ERROR");
						error.put("reasonCD", "005");
						error.put("subCode", "007");
						error.put("reasonTXT", "Signature rules error");
						logger.error("005-007-missing signature parameter:signatureRules.width");
						error.put("subReasonTXT", "missing signature parameter:signatureRules.width");
						errors.add(error);
					}
					try {
						String offset = signatureRule.get("offset").toString();
						if (offset.indexOf(",") < 0) {
							error = new HashMap<String, String>();
							error.put("errorType", "ERROR");
							error.put("reasonCD", "005");
							error.put("subCode", "008");
							error.put("reasonTXT", "Signature rules error");
							logger.error("005-008-signature parameter:signatureRules.offset is not in expected format");
							error.put("subReasonTXT", "signature parameter:signatureRules.offset is not in expected format");
							errors.add(error);
						} else {
							try {
								Integer.parseInt(signatureRule.get("offset").toString().split(",")[0]);
								Integer.parseInt(signatureRule.get("offset").toString().split(",")[1]);
							} catch (NumberFormatException ex) {
								error = new HashMap<String, String>();
								error.put("errorType", "ERROR");
								error.put("reasonCD", "005");
								error.put("subCode", "009");
								error.put("reasonTXT", "Signature rules error");
								logger.error("005-009-signature parameter:signatureRules.offset is not in expected format");
								error.put("subReasonTXT", "signature parameter:signatureRules.offset is not in expected format");
								errors.add(error);
							}
						}
					} catch (org.json.JSONException ex) {
						error = new HashMap<String, String>();
						error.put("errorType", "ERROR");
						error.put("reasonCD", "005");
						error.put("subCode", "010");
						error.put("reasonTXT", "Signature rules error");
						logger.error("005-010-missing signature parameter:signatureRules.offset");
						error.put("subReasonTXT", "missing signature parameter:signatureRules.offset");
						errors.add(error);
					}
//					try {
//						String anchor = signatureRule.get("anchor").toString();
//						if (!anchor.equalsIgnoreCase("top-left") && !anchor.equalsIgnoreCase("top-right")
//								&& !anchor.equalsIgnoreCase("bottom-left")
//								&& !anchor.equalsIgnoreCase("bottom-right")) {
//							error = new HashMap<String, String>();
//							error.put("errorType", "ERROR");
//							error.put("reasonCD", "005");
//							error.put("subCode", "011");
//							error.put("reasonTXT", "Signature rules error");
//							logger.debug("005-011-signature parameter:signatureRules.anchor is not in expected format");
//							error.put("subReasonTXT", "signature parameter:signatureRules.anchor is not in expected format");
//							errors.add(error);
//						}
//					} catch (org.json.JSONException ex) {
//						error = new HashMap<String, String>();
//						error.put("errorType", "ERROR");
//						error.put("reasonCD", "005");
//						error.put("subCode", "012");
//						error.put("reasonTXT", "Signature rules error");
//						logger.debug("005-012-missing signature parameter:signatureRules.anchor");
//						error.put("subReasonTXT", "missing signature parameter:signatureRules.anchor");
//						errors.add(error);
//					}
					try {
						String origin = signatureRule.get("origin").toString();
						if (!origin.equalsIgnoreCase("top-left") && !origin.equalsIgnoreCase("top-right")
								&& !origin.equalsIgnoreCase("bottom-left")
								&& !origin.equalsIgnoreCase("bottom-right")) {
							error = new HashMap<String, String>();
							error.put("errorType", "ERROR");
							error.put("reasonCD", "005");
							error.put("subCode", "013");
							error.put("reasonTXT", "Signature rules error");
							logger.error("005-013-signature parameter:signatureRules.origin is not in expected format");
							error.put("subReasonTXT", "signature parameter:signatureRules.origin is not in expected format");
							errors.add(error);
						}
					} catch (org.json.JSONException ex) {
						error = new HashMap<String, String>();
						error.put("errorType", "ERROR");
						error.put("reasonCD", "005");
						error.put("subCode", "014");
						error.put("reasonTXT", "Signature rules error");
						logger.error("005-014-missing signature parameter:signatureRules.origin");
						error.put("subReasonTXT", "missing signature parameter:signatureRules.origin");
						errors.add(error);
					}
				}
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "006");
				error.put("subCode", "002");
				error.put("reasonTXT", "Signature rules error");
				logger.error("006-002-missing signature parameter:signatureRules");
				error.put("subReasonTXT", "missing signature parameter:signatureRules");
				errors.add(error);
			}
		}else if (validateType == 2) {
			try {
				inputJson.get("sessionID");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "003");
				error.put("reasonTXT", "Incorrect json format");
				error.put("subReasonTXT", "missing parameter:sessionID");
				logger.error("007-003-missing parameter:sessionID");
				errors.add(error);
			}
			try {
				String strPageIndex  = inputJson.getString("pageIndex");
				try {
				Float.parseFloat(strPageIndex);
				}catch (NumberFormatException ex) {
					error = new HashMap<String, String>();
					error.put("errorType", "ERROR");
					error.put("reasonCD", "007");
					error.put("subCode", "004");
					error.put("reasonTXT", "Incorrect json format");
					logger.error("007-004-parameter:pageIndex not a number");
					error.put("subReasonTXT", "parameter:pageIndex not a number");
					errors.add(error);
				}
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "004");
				error.put("reasonTXT", "Incorrect json format");
				error.put("subReasonTXT", "missing parameter:pageIndex");
				logger.error("007-004-missing parameter:pageIndex");
				errors.add(error);
			}
			
		}else if (validateType == 3) {
			try {
				inputJson.get("imgStore");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "005");
				error.put("reasonTXT", "Incorrect json format");
				error.put("subReasonTXT", "missing signature parameter:imgStore");
				logger.debug("007-005-missing signature parameter:imgStore");
				errors.add(error);
			}
			
			try {
				inputJson.get("sessionID");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "003");
				error.put("reasonTXT", "Incorrect json format");
				error.put("subReasonTXT", "missing parameter:sessionID");
				logger.error("007-003-missing parameter:sessionID");
				errors.add(error);
			}
		}else if (validateType == 4) {
			try {
				inputJson.get("sessionID");
			} catch (org.json.JSONException ex) {
				error = new HashMap<String, String>();
				error.put("errorType", "ERROR");
				error.put("reasonCD", "007");
				error.put("subCode", "003");
				error.put("reasonTXT", "Incorrect json format");
				error.put("subReasonTXT", "missing parameter:sessionID");
				logger.error("007-003-missing parameter:sessionID");
				errors.add(error);
			}
		}
		return errors;
	}
	public static String calBase64FileSize(String imageBase64Str) {
		
		Integer equalIndex= imageBase64Str.indexOf("=");
		if(imageBase64Str.indexOf("=")>0) {
			imageBase64Str=imageBase64Str.substring(0, equalIndex);
		}
		Integer strLength=imageBase64Str.length();
		Integer size=strLength-(strLength/8)*2;
		
		BigDecimal filesize = new BigDecimal(size);  
		BigDecimal megabyte = new BigDecimal(1024 * 1024);  
		@SuppressWarnings("deprecation")
		float returnValue = filesize.divide(megabyte, 1, BigDecimal.ROUND_DOWN).floatValue();  
		if (returnValue > 1) {
			return (returnValue + "MB");
		}
		BigDecimal kilobyte = new BigDecimal(1024);
		returnValue = filesize.divide(kilobyte, 1, BigDecimal.ROUND_DOWN).floatValue();  
		return (returnValue + "KB");
	}
	public static void saveJSONToHD(JSONObject jsonString, String resultFileName) throws IOException {
		logger.debug("saveJSONToHD start");
		File file = null;
		Writer write = null;
		try{
			file = new File(resultFileName);
			if(file.exists())
				file.delete();
			write = new OutputStreamWriter(new FileOutputStream(resultFileName), "UTF-8");
			write.write(jsonString.toString());
			write.flush();
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}finally {
			file = null;
			write.close();
			write = null;
		}
		logger.debug("saveJSONToHD start");
	}
}