package com.eab.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class EnvVariable {

	private static Map<String, String> defaultVars = new HashMap<String, String>();

	private static void init() {
		defaultVars = new HashMap<String, String>();
		defaultVars.put("ENVIRONMENT_NAME", "DEV");
		defaultVars.put("removePreSignImage", "true");
		defaultVars.put("sys.analyzePDFAsync", "true");
		
	}

	public static String get(String name) {
		if (defaultVars == null || defaultVars.size() == 0) {
			init();
		}

		String result = System.getenv(name);
		if (result == null) {
			result = defaultVars.get(name);
		}
		return result;
	}
	public static String set(String key,String value) {
		if (defaultVars == null || defaultVars.size() == 0) {
			init();
		}

		String result = System.getenv(key);
		if (result == null) {
			result = defaultVars.put(key, value);
		}
		return result;
	}
	public static void main(String args[]) {
		String ttt = "1453455px";
		System.out.println(ttt.indexOf("px")+","+ttt.length());
	}
}
