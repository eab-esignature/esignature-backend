package com.eab.pdf;

import java.awt.Rectangle;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.pdfbox.text.TextPosition;

public class PdfBoxKeyWordPosition extends PDFTextStripper {
	private Logger logger = Logger.getLogger(PdfBoxKeyWordPosition.class);
	private char[] key;
	private String pdfPath;
	private List<float[]> list = new ArrayList<float[]>();
	private List<float[]> pagelist = new ArrayList<float[]>();
	private List<float[]> finalPageList = new ArrayList<float[]>();
	private String anchor = null;
	private String keyWords = "";

	public String getAnchor() {
		return anchor;
	}

	public void setAnchor(String anchor) {
		this.anchor = anchor;
	}

	public PdfBoxKeyWordPosition(String pKeyWords, String pdfPath) throws IOException {
		super();
		super.setSortByPosition(true);
//		this.setWordSeparator(" ");
		this.keyWords = pKeyWords;
		this.pdfPath = pdfPath;
		int firstWordLength = pKeyWords.length();
		if (pKeyWords.indexOf(" ") > 0)
			firstWordLength = pKeyWords.substring(0, pKeyWords.indexOf(" ")).length();
		char[] key = new char[firstWordLength];
		for (int i = 0; i < firstWordLength; i++) {
			key[i] = pKeyWords.charAt(i);
//			System.out.println("key[i] = "+key[i]);
		}
		this.key = key;
	}

	public char[] getKey() {
		return key;
	}

	public void setKey(char[] key) {
		this.key = key;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public List<float[]> getCoordinate() throws IOException {
		try {
			document = PDDocument.load(new File(pdfPath));
			int pages = document.getNumberOfPages();
			for (int i = 1; i <= pages; i++) {
				pagelist.clear();
				super.setSortByPosition(true);
				super.setStartPage(i);
				super.setEndPage(i);
				Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
				this.writeText(document, dummy);
//				boolean found = false;
				for (float[] li : pagelist) {
					li[2] = i;
					if (keyWords.indexOf(" ") > 0) {
//						String firstWord = keyWords.substring(0,keyWords.indexOf(" "));
						int iX = Integer
								.parseInt(String.valueOf(li[0]).substring(0, String.valueOf(li[0]).indexOf(".")));
						int iY = Integer
								.parseInt(String.valueOf(li[3]).substring(0, String.valueOf(li[3]).indexOf(".")));
						PDPage page = document.getPage(i - 1);
						PDFTextStripperByArea stripper = new PDFTextStripperByArea();
						stripper.setSortByPosition(true);
						Rectangle rect1 = new Rectangle(iX, iY, (int) page.getBBox().getWidth(), 10);
						stripper.addRegion("class1", rect1);
						stripper.extractRegions(page);
						String textForRegion = stripper.getTextForRegion("class1");
//						System.out.println("textForRegion = "+textForRegion);
//						System.out.println("textForRegion.indexOf(keyWords) = "+textForRegion.indexOf(keyWords));
//						System.out.println("li[2] = "+li[2]);
						if (textForRegion != null && !"".equals(textForRegion.trim())) {
							if (textForRegion.indexOf(keyWords) == 0) {
								finalPageList.add(li);
							}
						}
					} 
				}				
				if (keyWords.indexOf(" ") < 0)
					list.addAll(pagelist);
			}
			if (keyWords.indexOf(" ") >= 0)
				list.addAll(finalPageList);
			
			return list;
		} catch (IOException e) {
			logger.error(e.toString());
			throw e;
		} finally {
			if (document != null) {
				document.close();
			}
		}
	}

	@Override
	protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
		for (int i = 0; i < textPositions.size(); i++) {
//			System.out.println("textPositions = "+textPositions.toString());
			String text = textPositions.toString().replaceAll("[^\u4E00-\u9FA5]", "");
			String str = textPositions.get(i).getUnicode();
//			System.out.println("key[0] = "+key[0]+",text.length() = "+text.length());
			if (str.equals(key[0] + "") && text.length() < 10) {
				int count = 0;
				for (int j = 0; j < key.length; j++) {
					String s = "";
					try {
						s = textPositions.get(i + j).getUnicode();
//						System.out.println("s["+(i+j)+"]"+s);
					} catch (Exception e) {
						s = "";
					}
					if (s.equals(key[j] + "")) {
//						System.out.println("key["+key[j]+"]"+s);
						count++;
					}
				}
				if (count == key.length) {
//					float[] idx = new float[3];
					float[] idx = new float[4];
					idx[0] = textPositions.get(i).getX();
					idx[1] = textPositions.get(i).getPageHeight() - textPositions.get(i).getY();
					idx[3] = textPositions.get(i).getY();
					pagelist.add(idx);
				}
			}
		}
	}

	public static void main(String[] args) {
		try {
//			PdfBoxKeyWordPosition pdfBoxKeyWordPosition = new PdfBoxKeyWordPosition("Total Initial Annual Premium","F:\\pdfconvertion\\demo_eng.pdf");
			PdfBoxKeyWordPosition pdfBoxKeyWordPosition = new PdfBoxKeyWordPosition("Signature",
					"F:\\pdfconvertion\\demo_eng.pdf");
//			PdfBoxKeyWordPosition pdfBoxKeyWordPosition = new PdfBoxKeyWordPosition("Signature","F:\\pdfconvertion\\demo_eng.pdf");
//			pdfBoxKeyWordPosition.setWordSeparator("\b");
			List result = pdfBoxKeyWordPosition.getCoordinate();
			if (result != null && result.size() > 0) {
				for (int i = 0; i < result.size(); i++) {
					float[] position = (float[]) result.get(i);
					
					for(int j=0;j<position.length;j++) {
						System.out.println(position[j]);
					}
				}
			}

//			replacePic(args);
//			PDDocument d = PDDocument.load(new File("F:\\pdfconvertion\\demo_eng.pdf"));
//			PDPage page = d.getPage(6);
//			PDFTextStripperByArea stripper = new PDFTextStripperByArea();
//			stripper.setSortByPosition( true );
////			Rectangle rect1 = new Rectangle( 0, 600, 200, 10 );
//
//					Rectangle rect1 = new Rectangle( 57, 678, (int)page.getBBox().getWidth(),100);
//		//			Rectangle rect1 = new Rectangle( 0, 610, 86, 10 );
//					stripper.addRegion( "class1", rect1 );
////					stripper.setStartPage(7);
////					stripper.setEndPage(7);
//					stripper.extractRegions(page);
//
//					
//		//			System.out.print(stripper.getTextForRegion( "class1" )+stripper.getTextForRegion( "class2" )+stripper.getTextForRegion( "class3" )+stripper.getTextForRegion( "class4" )+stripper.getTextForRegion( "class5" )+stripper.getTextForRegion( "class6" ) );
//					if(stripper.getTextForRegion( "class1" )!=null && !"".equals(stripper.getTextForRegion( "class1" ).trim()))						
//						System.out.print(stripper.getTextForRegion( "class1" ).indexOf("confirm having read"));
//		//			System.out.println(stripper.getTextForRegion( "class1" ).indexOf(test));
//
//			
//			
////			System.out.println(rect1.getX());
////			System.out.println(rect1.getY());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void replacePic(String[] args) throws Exception {
		String pdfPath = "F:\\pdfconvertion\\demo_eng.pdf";
		File file = new File(pdfPath);
		PDDocument doc = PDDocument.load(file);
//		String keyWords = "BENEFIT\u0020SUMMARY";
//		String keyWords = "��Ո����";
		String keyWords = "Signature";
		PDImageXObject pdImage = PDImageXObject.createFromFile("F:\\pdfconvertion\\timg2.png", doc);
		PdfBoxKeyWordPosition pdf = new PdfBoxKeyWordPosition(keyWords, pdfPath);
		PDPageContentStream contentStream = null;
		List<float[]> list = pdf.getCoordinate();
		for (float[] fs : list) {
			PDPage page = doc.getPage((int) fs[2] - 1);
			float x = fs[0];
			float y = fs[1];
//			System.out.println("page = " + fs[2]);
			contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, false, false);
			contentStream.drawImage(pdImage, x, y);
//			contentStream.drawImage(pdImage, x, y,pdImage.getWidth(),pdImage.getHeight());
			contentStream.close();
		}
		doc.save("F:\\pdfconvertion\\demo_chi_result.pdf");
		doc.close();
	}
}
