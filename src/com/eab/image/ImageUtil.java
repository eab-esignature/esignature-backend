package com.eab.image;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.eab.common.EnvVariable;
import com.eab.common.UserConfigVariable;
import com.eab.pdf.IcepdfConvertor;
import com.eab.pdf.PDFUtil;

public class ImageUtil {
	private Logger logger = Logger.getLogger(ImageUtil.class);
	protected String fileAnalysisLocation = null;
	protected String fileAnalysisSrcLocatoin = null;
	protected String fileAnalysisWorkingLocatoin = null;
	protected String fileAnalysisResultLocatoin = null;
	protected String imageScaling = null;
	protected String imageRotation = null;
	protected JSONObject eSignatureRule = null;

	public ImageUtil() throws IOException {
		if (UserConfigVariable.get("user.pdfRootPath") != null
				&& !"".equals(UserConfigVariable.get("user.pdfRootPath")))
			fileAnalysisLocation = UserConfigVariable.get("user.pdfRootPath");
		fileAnalysisSrcLocatoin = UserConfigVariable.get("user.pdfSourcePath");
		fileAnalysisWorkingLocatoin = UserConfigVariable.get("user.pdfProcessPath");
		fileAnalysisResultLocatoin = UserConfigVariable.get("user.pdfResultPath");
		imageScaling = EnvVariable.get("sys.imageScaling");
		imageRotation = EnvVariable.get("sys.imageRotation");

		if (UserConfigVariable.get("user.pdfSourcePath") == null
				|| "".equals(UserConfigVariable.get("user.pdfSourcePath")))
			fileAnalysisSrcLocatoin = "/";
		if (UserConfigVariable.get("user.pdfProcessPath") == null
				|| "".equals(UserConfigVariable.get("user.pdfProcessPath")))
			fileAnalysisWorkingLocatoin = "/";
		if (UserConfigVariable.get("user.pdfResultPath") == null
				|| "".equals(UserConfigVariable.get("user.pdfResultPath")))
			fileAnalysisResultLocatoin = "/";
		if (EnvVariable.get("sys.imageScaling") == null || "".equals(EnvVariable.get("sys.imageScaling")))
			imageScaling = "1.0f";
		if (EnvVariable.get("sys.imageRotation") == null || "".equals(EnvVariable.get("sys.imageRotation")))
			imageRotation = "0f";

	}

	public String localImageToBASE64(String sessionID,String pageIndex) throws Exception {
		String filePath = this.getFileAnalysisLocation() + "\\" + sessionID + "\\"
				+ this.getFileAnalysisWorkingLocatoin();
		String srcFilePath = this.getFileAnalysisLocation() + "\\" + sessionID + "\\"
				+ this.getFileAnalysisSrcLocatoin();
		
		String input = FileUtils.readFileToString(new File(srcFilePath+"\\"+PDFUtil.inputParamJsonFileName), "UTF-8");
		JSONObject jsonObject = new JSONObject(input);
		String docID = jsonObject.getString("docID");
		return this.image2BASE64(filePath+"\\"+docID+"_"+pageIndex+".jpg");
	}

	public String image2BASE64(String filePath) throws IOException {
		String imageString = null;
		byte[] data = null;
		try (InputStream in = new FileInputStream(filePath)) {
			data = new byte[in.available()];
			in.read(data);
			imageString = Base64.encodeBase64String(data);
		} catch (IOException e) {
			logger.error(e.toString());
			throw e;
		}
		return imageString;
	}

	public String getFileAnalysisLocation() {
		return fileAnalysisLocation;
	}

	public void setFileAnalysisLocation(String fileAnalysisLocation) {
		this.fileAnalysisLocation = fileAnalysisLocation;
	}

	public String getFileAnalysisSrcLocatoin() {
		return fileAnalysisSrcLocatoin;
	}

	public void setFileAnalysisSrcLocatoin(String fileAnalysisSrcLocatoin) {
		this.fileAnalysisSrcLocatoin = fileAnalysisSrcLocatoin;
	}

	public String getFileAnalysisWorkingLocatoin() {
		return fileAnalysisWorkingLocatoin;
	}

	public void setFileAnalysisWorkingLocatoin(String fileAnalysisWorkingLocatoin) {
		this.fileAnalysisWorkingLocatoin = fileAnalysisWorkingLocatoin;
	}

	public String getFileAnalysisResultLocatoin() {
		return fileAnalysisResultLocatoin;
	}

	public void setFileAnalysisResultLocatoin(String fileAnalysisResultLocatoin) {
		this.fileAnalysisResultLocatoin = fileAnalysisResultLocatoin;
	}

	public String getImageScaling() {
		return imageScaling;
	}

	public void setImageScaling(String imageScaling) {
		this.imageScaling = imageScaling;
	}

	public String getImageRotation() {
		return imageRotation;
	}

	public void setImageRotation(String imageRotation) {
		this.imageRotation = imageRotation;
	}

	public JSONObject geteSignatureRule() {
		return eSignatureRule;
	}

	public void seteSignatureRule(JSONObject eSignatureRule) {
		this.eSignatureRule = eSignatureRule;
	}

	// #data:image/jpeg;base64,
	public JSONObject base64ToJson(String strBase64, String header) {
		JSONObject result = new JSONObject();
		result.put("data", header + strBase64);
		return result;
	}

	public boolean base64StringToImage(String base64String, String fileName) {
		boolean success = false;
		try {
			byte[] bytes1 = Base64.decodeBase64(base64String);
			ByteArrayInputStream bais = new ByteArrayInputStream(bytes1);
			BufferedImage bi1 = ImageIO.read(bais);
			File f1 = new File(fileName);
			ImageIO.write(bi1, "png", f1);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

	public boolean base64StringToImage(String base64String, String fileName, int height, int width) {
		boolean success = false;
		try {
//            byte[] bytes1 = Base64.decodeBase64(base64String);
//            ByteArrayInputStream bais = new ByteArrayInputStream(bytes1);
//            BufferedImage bi1 = ImageIO.read(bais);
//            File f1 = new File(fileName);
//            ImageIO.write(bi1, "png", f1);
			OutputStream os = new FileOutputStream(fileName);
			OutputStream os1 = new FileOutputStream(fileName);
			String format = fileName.substring(fileName.indexOf(".") + 1);
			ByteArrayInputStream in = new ByteArrayInputStream(Base64.decodeBase64(base64String));
			BufferedImage prevImage = ImageIO.read(in);
//            double preWidth = prevImage.getWidth();
//            double preHeight = prevImage.getHeight();

//            int tempWidth = (int) (preWidth / 1) ;
//            int tempHeight = (int) (preHeight / 1) ;

			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
			Graphics graphics = image.createGraphics();
			graphics.drawImage(prevImage, 0, 0, width, height, null);
			ImageIO.write(prevImage, format, os1);
			ImageIO.write(image, format, os);
			os.flush();

			in.close();
			os.close();
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

	public static void main(String[] arg) {
	}

}
