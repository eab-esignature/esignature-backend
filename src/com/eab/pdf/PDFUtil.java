package com.eab.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.UserConfigVariable;
import com.eab.image.ImageUtil;

public class PDFUtil {
	private Logger logger = Logger.getLogger(PDFUtil.class);
	public static final String analysisJsonFileName = "ProcessResult.json";
	public static final String inputParamJsonFileName = "UserParameter.json";

	public PDFConvertResult proceed(String sessionID, JSONObject json) {
		PDFConvertResult result = new PDFConvertResult();
		HashMap<String, String> error = new HashMap<String, String>();
		try {
			PDFConvertor pDFConvertor = PDFConvertor.createPDFtools("EN");
//			pDFConvertor.saveJSONToHD(json, "eSignatureParam.json");
			String filePath = pDFConvertor.getFileAnalysisLocation() + "\\" + sessionID + "\\"
					+ pDFConvertor.getFileAnalysisSrcLocatoin() + "\\" + inputParamJsonFileName;
			pDFConvertor.seteSignatureRule(json);
			pDFConvertor.setSrcPDFName(json.getString("docID"));
			if (pDFConvertor.prepare(sessionID)) {
				Function.saveJSONToHD(json, filePath);
				if (pDFConvertor.base64StringToPDF(sessionID)) {
					if (pDFConvertor.convertPDFtoIMG(sessionID)) {
//						result = pDFConvertor.getConvertResult();
						if ("true".equals(EnvVariable.get("sys.analyzePDFAsync")))
							pDFConvertor.analyzePDFAsync(sessionID);
						else
							pDFConvertor.analyzePDF(sessionID);
						result = pDFConvertor.getConvertResult();
						result.setSuccess(true);
						result.setDocID(json.getString("docID"));
						result.setLocalization(json.getString("localization"));
						pDFConvertor.saveAnalyzeResult(sessionID, result);

					}
				}
//				if (pDFConvertor.base64StringToPDF(json.get("binary").toString(), sessionID,"_signed")) {
//					
//				}
			}
		} catch (IOException e) {
			if (e.getMessage().indexOf("Error: Header doesn't contain versioninfo") >= 0) {
				error.put("errorType", "ERROR");
				error.put("reasonCD", "002");
				error.put("subCode", "001");
				error.put("reasonTXT", "Incorrect file format");
				result.setSuccess(false);
				logger.debug("002-001-The file is not in PDF format");
			} else {
				error.put("errorType", "ERROR");
				error.put("reasonCD", "003");
				error.put("subCode", "001");
				error.put("reasonTXT", "Server error");
				result.setSuccess(false);
				logger.error("003-001-Exception in the eSign server");
			}
		} catch (Exception e) {
			error.put("errorType", "ERROR");
			error.put("reasonCD", "003");
			error.put("subCode", "001");
			error.put("reasonTXT", "Server error");
			result.setSuccess(false);
			logger.error("003-001-Exception in the eSign server");
		}
		result.setErrors(error);
		return result;
	}

	public boolean saveSignature(String sessionID, JSONObject jsonObject) throws Exception {
		boolean success = false;
		try {
			PDFConvertor pDFConvertor = PDFConvertor.createPDFtools("EN");
//			pDFConvertor.mergeSignToPDF(sessionID, jsonObject);
			if (pDFConvertor.saveSignatures2Imgs(sessionID, jsonObject)) {
				success = pDFConvertor.signPDF(sessionID, jsonObject);
			}
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
		return success;
	}

	public JSONObject loadAnalyzeResult(String sessionID) throws Exception {
		try {
			PDFConvertor pDFConvertor = PDFConvertor.createPDFtools("EN");
			String filePath = pDFConvertor.getFileAnalysisLocation() + "\\" + sessionID + "\\"
					+ pDFConvertor.getFileAnalysisWorkingLocatoin() + "\\" + analysisJsonFileName;
			String input = FileUtils.readFileToString(new File(filePath), "UTF-8");
			return new JSONObject(input);
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
	}

	public Object loadSignedPDF(String sessionID) throws Exception {
		try {
			JSONObject processResult = this.loadAnalyzeResult(sessionID);
			PDFConvertor pDFConvertor = PDFConvertor.createPDFtools("EN");
			String filePath = pDFConvertor.getFileAnalysisLocation() + "\\" + sessionID + "\\"
					+ pDFConvertor.getFileAnalysisResultLocatoin();
			if (processResult != null) {
				filePath = filePath + "\\" + processResult.getString("docID") + "_signed.pdf";
				if ("BASE64".equals(UserConfigVariable.get("user.signedPDFOutputType"))) {
					ImageUtil imageUtil = new ImageUtil();
					return imageUtil.image2BASE64(filePath);
				}else if("IO_STREAM".equals(UserConfigVariable.get("user.signedPDFOutputType"))) {
					return convertPDFToByteArrayOutputStream(filePath);
				}
			}
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
		return sessionID;
	}

	public static ByteArrayOutputStream convertPDFToByteArrayOutputStream(String filePath) {
		InputStream inputStream = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			inputStream = new FileInputStream(filePath);
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return baos;
	}
	public static void main(String[] args) {
		PDFUtil.convertPDFToByteArrayOutputStream("D:\\eSignatureAnalysis\\DC0A2814A085577571819CEF67CAEB2B\\NB-3243545366_signed.pdf");
	}
}
