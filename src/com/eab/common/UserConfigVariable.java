package com.eab.common;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class UserConfigVariable {
	private static Map defaultVars = new HashMap();
	public static Map<String, String> getDefaultVars() {
		return defaultVars;
	}
	private static void init() {
		defaultVars = new HashMap<String, String>();
		defaultVars.put("ENVIRONMENT_NAME", "DEV");
	}
	public static String get(String name) {
		if (defaultVars == null || defaultVars.size() == 0) {
			init();
		}

		String result = System.getenv(name);
		if (result == null) {
			result = (String)defaultVars.get(name);
		}
		return result;
	}
	public static String set(String key,Object value) {
		if (defaultVars == null || defaultVars.size() == 0) {
			init();
		}

		String result = System.getenv(key);
		if (result == null) {
			result = (String) defaultVars.put(key, value);
		}
		return result;
	}
	public static void main(String[] args) {
		
		if("true".equalsIgnoreCase(EnvVariable.get("removePreSignImage"))) {
		File[] preSaveSignPics = new File("D:\\eSignatureAnalysis\\8A4298A8F0007442F53B07392B9CA1E0").listFiles();
		if(preSaveSignPics!=null && preSaveSignPics.length>0) {
			Arrays.asList(preSaveSignPics).stream().forEach(x->{
				if(x.isFile()) {
					if(x.getName().indexOf("imgKey")>=0 && "png".equalsIgnoreCase(x.getName().substring(x.getName().indexOf(".")+1)))
						x.delete();
				}
					
			});
//			if(preSaveSignPic.isFile()) {
//				System.out.println(preSaveSignPic.getName());
//				if(preSaveSignPic.getName().indexOf("imgKey")>=0 && "png".equalsIgnoreCase(preSaveSignPic.getName().substring(preSaveSignPic.getName().indexOf(".")+1)))
//					//preSaveSignPic.delete();
//					System.out.println(preSaveSignPic.getName());
//			}
		}}
		
	}
}
