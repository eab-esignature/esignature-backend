package com.eab.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

public class SystemListener implements ServletContextListener {
	private Logger logger = Logger.getLogger(SystemListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		try (InputStream sysConfig = SystemListener.class.getClassLoader().getResourceAsStream("system_config.properties");
			InputStream userConfig = SystemListener.class.getClassLoader().getResourceAsStream("user_config.properties")	) {
			
			Properties sysConfigParam = new Properties();
			sysConfigParam.load(sysConfig);
			Enumeration<?> systemPropNames = sysConfigParam.propertyNames();
			while (systemPropNames.hasMoreElements()) {
				String key = (String) systemPropNames.nextElement();
				String value = sysConfigParam.getProperty(key, "");
				if (value.equals("{context_path}"))
					value = sce.getServletContext().getRealPath("/");
				if (value.equals("{ksFileName}"))
					value = "keystore.jks";
				EnvVariable.set(key, value);
				logger.error("System Config-->"+key + ":" + EnvVariable.get(key));
			}
			
			Properties userConfigParam = new Properties();
			userConfigParam.load(userConfig);
			Enumeration<?> userPropNames = userConfigParam.propertyNames();
			while (userPropNames.hasMoreElements()) {
				String key = (String) userPropNames.nextElement();
				String value = userConfigParam.getProperty(key, "");
				logger.error("User Config-->"+key + ":" + value);
				if("true".equalsIgnoreCase(value) )
					UserConfigVariable.set(key, true);
				else if("false".equalsIgnoreCase(value))
					UserConfigVariable.set(key, false);
				else
					UserConfigVariable.set(key, value);
//				logger.error("User Config-->"+key + ":" + UserConfigVariable.get(key));
			}
			
			logger.error("System initialization succeed");
		} catch (Throwable e) {
			logger.debug(e.toString());
			logger.error(e.toString());
			logger.error("System initialization failed");
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	}

}
