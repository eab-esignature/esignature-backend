package com.eab.pdf;

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

public class PDFConvertResult {
	private boolean success = false;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	private int totPageCnt = 0;

	public int getTotPageCnt() {
		return totPageCnt;
	}
	public void setTotPageCnt(int totPageCnt) {
		this.totPageCnt = totPageCnt;
	}
	private HashMap<String, String> errors = null;

	public HashMap<String, String> getErrors() {
		return errors;
	}
	public void setErrors(HashMap<String, String> errors) {
		this.errors = errors;
	}
	private List <HashMap<String, String>> resultList = null;
	public List<HashMap<String, String>> getResultList() {
		return resultList;
	}
	public void setResultList(List<HashMap<String, String>> resultList) {
		this.resultList = resultList;
	}
	private float pageHeight = 0;
	public float getPageHeight() {
		return pageHeight;
	}
	public void setPageHeight(float pageHeight) {
		this.pageHeight = pageHeight;
	}
	public float getPageWidth() {
		return pageWidth;
	}
	public void setPageWidth(float pageWidth) {
		this.pageWidth = pageWidth;
	}
	private float pageWidth = 0;
	
	public JSONObject toJSONObject() {
		JSONObject jSONObject = new JSONObject();
		jSONObject.put("pageCount", this.totPageCnt);
		jSONObject.put("docID", this.getDocID());
		jSONObject.put("pageHeight", pageHeight+"pt");
		jSONObject.put("pageWidth", pageWidth+"pt");
		jSONObject.put("desc", "Description");
		jSONObject.put("signatureFields", resultList);
		jSONObject.put("Status", this.success?"Success":"Fail");
		jSONObject.put("localization", localization);
		return jSONObject;
	}
	private String docID = null;
	public String getDocID() {
		return docID;
	}
	public void setDocID(String docID) {
		this.docID = docID;
	}
	private String localization = null;
	public String getLocalization() {
		return localization;
	}
	public void setLocalization(String localization) {
		this.localization = localization;
	}
}
