package com.eab.pdf;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.UserConfigVariable;
import com.eab.image.ImageUtil;

public abstract class PDFConvertor {
	private static Logger logger = Logger.getLogger(PDFConvertor.class);
	
	protected String fileAnalysisLocation = null;
	public String getFileAnalysisLocation() {
		return fileAnalysisLocation;
	}

	public void setFileAnalysisLocation(String fileAnalysisLocation) {
		this.fileAnalysisLocation = fileAnalysisLocation;
	}

	public String getFileAnalysisSrcLocatoin() {
		return fileAnalysisSrcLocatoin;
	}

	public void setFileAnalysisSrcLocatoin(String fileAnalysisSrcLocatoin) {
		this.fileAnalysisSrcLocatoin = fileAnalysisSrcLocatoin;
	}

	public String getFileAnalysisWorkingLocatoin() {
		return fileAnalysisWorkingLocatoin;
	}

	public void setFileAnalysisWorkingLocatoin(String fileAnalysisWorkingLocatoin) {
		this.fileAnalysisWorkingLocatoin = fileAnalysisWorkingLocatoin;
	}

	public String getFileAnalysisResultLocatoin() {
		return fileAnalysisResultLocatoin;
	}

	public void setFileAnalysisResultLocatoin(String fileAnalysisResultLocatoin) {
		this.fileAnalysisResultLocatoin = fileAnalysisResultLocatoin;
	}

	public String getImageScaling() {
		return imageScaling;
	}

	public void setImageScaling(String imageScaling) {
		this.imageScaling = imageScaling;
	}

	public String getImageRotation() {
		return imageRotation;
	}

	public void setImageRotation(String imageRotation) {
		this.imageRotation = imageRotation;
	}

	public void setSignedPDFName(String signedPDFName) {
		this.signedPDFName = signedPDFName;
	}

	protected String fileAnalysisSrcLocatoin = null;
	protected String fileAnalysisWorkingLocatoin = null;
	protected String fileAnalysisResultLocatoin = null;
	protected String imageScaling = null;
	protected String imageRotation = null;
	protected JSONObject eSignatureRule = null;
	protected PDFConvertResult convertResult = null;

	public PDFConvertResult getConvertResult() {
		return convertResult;
	}

	public void setConvertResult(PDFConvertResult convertResult) {
		this.convertResult = convertResult;
	}

	public JSONObject geteSignatureRule() {
		return eSignatureRule;
	}

	public void seteSignatureRule(JSONObject eSignatureRule) {
		this.eSignatureRule = eSignatureRule;
	}

	protected String srcPDFName = null;

	public void setSrcPDFName(String srcPDFName) {
		this.srcPDFName = srcPDFName;
	}

	public String getSrcPDFName() {
		return srcPDFName;
	}

	public String getSignedPDFName() {
		return signedPDFName;
	}

	protected String signedPDFName = null;

	public boolean convertPDFtoIMG(String sessionID) throws Exception {
		String filePath = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisSrcLocatoin + "\\"
				+ this.srcPDFName+".pdf";
		String workingFolder = this.fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisWorkingLocatoin;
		if("true".equals(EnvVariable.get("sys.convertIMGAsync")))
			return this.pdf2imgAsync(filePath, workingFolder);
		else
			return  this.pdf2img(filePath, workingFolder);
		
	}

	public abstract boolean pdf2img(String srcFileName, String targetFilePath) throws Exception;
	public abstract boolean pdf2imgAsync(String srcFileName, String targetFilePath) throws Exception;
	public abstract void analyzePDF(String sessionID) throws Exception;
	public abstract void analyzePDFAsync(String sessionID) throws Exception;
	public abstract boolean mergeSignToPDF(String sessionID, JSONObject jsonObject) throws Exception;
	public abstract boolean signPDF(String sessionID, JSONObject jsonObject) throws Exception;
	public abstract List<float[]> findKeyWordPosition(String lookupKey, String filePath ) throws Exception;

	public PDFConvertor() throws IOException {
		if (UserConfigVariable.get("user.pdfRootPath") != null
				&& !"".equals(UserConfigVariable.get("user.pdfRootPath")))
			fileAnalysisLocation = UserConfigVariable.get("user.pdfRootPath");
		fileAnalysisSrcLocatoin = UserConfigVariable.get("user.pdfSourcePath");
		fileAnalysisWorkingLocatoin = UserConfigVariable.get("user.pdfProcessPath");
		fileAnalysisResultLocatoin = UserConfigVariable.get("user.pdfResultPath");
		imageScaling = EnvVariable.get("sys.imageScaling");
		imageRotation = EnvVariable.get("sys.imageRotation");

		if (UserConfigVariable.get("user.pdfSourcePath") == null
				|| "".equals(UserConfigVariable.get("user.pdfSourcePath")))
			fileAnalysisSrcLocatoin = "/";
		if (UserConfigVariable.get("user.pdfProcessPath") == null
				|| "".equals(UserConfigVariable.get("user.pdfProcessPath")))
			fileAnalysisWorkingLocatoin = "/";
		if (UserConfigVariable.get("user.pdfResultPath") == null
				|| "".equals(UserConfigVariable.get("user.pdfResultPath")))
			fileAnalysisResultLocatoin = "/";
		if (EnvVariable.get("sys.imageScaling") == null || "".equals(EnvVariable.get("sys.imageScaling")))
			imageScaling = "1.0f";
		if (EnvVariable.get("sys.imageRotation") == null || "".equals(EnvVariable.get("sys.imageRotation")))
			imageRotation = "0f";

//		this.srcPDFName = "eSignatureSrc.pdf";
//		this.signedPDFName = "eSignature_signed.pdf";
		this.convertResult = new PDFConvertResult();
	}

	public boolean prepare(String sessionID) throws Exception {
		try {
			File rootFolder = new File(fileAnalysisLocation);
			if (!rootFolder.exists())
				rootFolder.mkdir();
			File sessionFolder = new File(fileAnalysisLocation + "\\" + sessionID);
			if (!sessionFolder.exists())
				sessionFolder.mkdirs();
			File srcFolder = new File(fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisSrcLocatoin);
			if (!srcFolder.exists())
				srcFolder.mkdirs();
			File wrokingFolder = new File(fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisWorkingLocatoin);
			if (!wrokingFolder.exists())
				wrokingFolder.mkdirs();
			File resultFolder = new File(fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisResultLocatoin);
			if (!resultFolder.exists())
				resultFolder.mkdirs();
			return true;
		} catch (Exception e) {
			logger.error(e.toString());
			return false;
		}
	}

	public static PDFConvertor createPDFtools(String lang)
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		try {
			String className = "";
			if (lang.equals("CN"))
				className = EnvVariable.get("sys.pdftools.chi");
			else
				className = EnvVariable.get("sys.pdftools.eng");
			if (className == null || "".equals(className))
				className = "com.eab.pdf.PDFBoxConvertor";
			@SuppressWarnings("unchecked")
			Class<PDFConvertor> pdfTools = (Class<PDFConvertor>) Class.forName(className);
			Constructor<PDFConvertor> constructor = pdfTools.getConstructor();
			return constructor.newInstance();
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
	}

	public boolean base64StringToPDF(String sessionID) throws Exception {
		logger.debug("base64StringToPDF start");
		File pdf = new File(fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisSrcLocatoin + "\\"
				+ this.getSrcPDFName() + ".pdf");
//		String fileSize = Function.calBase64FileSize(this.eSignatureRule.get("binary").toString());
//		System.out.println("pdfSize = " + fileSize);
		try (ByteArrayInputStream bais = new ByteArrayInputStream(
				Base64.decodeBase64(this.eSignatureRule.get("binary").toString()));
				BufferedInputStream bin = new BufferedInputStream(bais);
				FileOutputStream fout = new FileOutputStream(pdf);
				BufferedOutputStream bout = new BufferedOutputStream(fout)) {
			byte[] buffers = new byte[1024];
			int len = bin.read(buffers);
			while (len != -1) {
				bout.write(buffers, 0, len);
				len = bin.read(buffers);
			}
			bout.flush();
			logger.debug("base64StringToPDF end");
			return true;
		} catch (Exception e) {
			logger.error(e.toString());
			throw e;
		}
	}

	public boolean saveResult(String sessionID, PDFConvertResult convertResult) throws IOException {

		String pdf = fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisResultLocatoin
				+ "\\eSignatureResult.json";
		return saveResultToHD(convertResult, pdf);
	}

	public boolean saveResultToHD(PDFConvertResult convertResult, String resultFileName) throws IOException {
		JSONObject jsonString = convertResult.toJSONObject();
		Function.saveJSONToHD(jsonString, resultFileName);
		return true;
	}

	public boolean saveAnalyzeResult(String sessionID, PDFConvertResult convertResultin) throws IOException {

		String pdf = fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisWorkingLocatoin
				+ "\\"+PDFUtil.analysisJsonFileName;
		JSONObject jsonString = convertResultin.toJSONObject();
		Function.saveJSONToHD(jsonString, pdf);
		return true;
	}

	public boolean saveAnalyzeResult(String sessionID, JSONObject jsonString) throws IOException {

		String pdf = fileAnalysisLocation + "\\" + sessionID + "\\" + fileAnalysisWorkingLocatoin
				+ "\\"+PDFUtil.analysisJsonFileName;
		Function.saveJSONToHD(jsonString, pdf);
		return true;
	}

	public boolean saveSignatures2Imgs(String sessionID, JSONObject param) throws IOException {
		ImageUtil imageUtil = new ImageUtil();
		JSONArray imageArray = null;
		try {
			String imageFolder = fileAnalysisLocation + "\\" + sessionID + "\\" + this.fileAnalysisResultLocatoin;
			if("true".equalsIgnoreCase(EnvVariable.get("removePreSignImage"))) {
			File[] preSaveSignPics = new File(imageFolder).listFiles();
			if(preSaveSignPics!=null && preSaveSignPics.length>0) {
				Arrays.asList(preSaveSignPics).stream().forEach(preSaveSignPic ->{
					if(preSaveSignPic.isFile()) {
						if(preSaveSignPic.getName().indexOf("imgKey")>=0 && "png".equalsIgnoreCase(preSaveSignPic.getName().substring(preSaveSignPic.getName().indexOf(".")+1)))
							preSaveSignPic.delete();
					}					
				});
			}}
			imageArray = param.getJSONArray("imgStore");
			if (imageArray != null && imageArray.length() > 0) {
				for (int i = 0; i < imageArray.length(); i++) {
					JSONObject imagesBase64 = imageArray.getJSONObject(i);
					String strH = imagesBase64.getString("height");
					String strW = imagesBase64.getString("width");
					float fH = Float.parseFloat(strH);
					float fW = Float.parseFloat(strW);
					fH = fH * 4 / 3;
					fW = fW * 4 / 3;
					strH = String.valueOf(fH);
					strW = String.valueOf(fW);
					int height = strH.indexOf(".") > 0 ? Integer.parseInt(strH.substring(0, strH.indexOf(".")))
							: Integer.parseInt(strH);
					int width = strW.indexOf(".") > 0 ? Integer.parseInt(strW.substring(0, strW.indexOf(".")))
							: Integer.parseInt(strW);
					String fileName = imageFolder + "\\" + imagesBase64.getString("key") + ".png";
//					imageUtil.base64StringToImage(imagesBase64.getString("data").replace("data:image/png;base64,", ""),
//							fileName, height * 150 /100, width* 150 /100); //zoom in 150%,because signature box zoom out 50%
					imageUtil.base64StringToImage(imagesBase64.getString("data").replace("data:image/png;base64,", ""),
							fileName); //zoom in 150%,because signature box zoom out 50%
				}
			} else {
				throw new IOException("there is no any signature image");
			}
			return true;
		} catch (Exception ex) {
			logger.error(ex.toString());
			throw ex;
		}
	}

}
